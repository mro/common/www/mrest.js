---
parserOptions:
  sourceType: module
  ecmaVersion: 11

env:
  amd: true
  browser: true
  jquery: true
  node: true

extends:
  - "eslint:recommended"

# http://eslint.org/docs/rules/
rules:
  # Possible Errors
  no-await-in-loop: off
  no-cond-assign: error
  no-console: off
  no-constant-condition: error
  no-control-regex: error
  no-debugger: error
  no-dupe-args: error
  no-dupe-keys: error
  no-duplicate-case: error
  no-empty-character-class: error
  no-empty: error
  no-ex-assign: error
  no-extra-boolean-cast: error
  no-extra-parens: off
  no-extra-semi: error
  no-func-assign: error
  no-inner-declarations:
    - error
    - functions
  no-invalid-regexp: error
  no-irregular-whitespace: error
  no-negated-in-lhs: error
  no-obj-calls: error
  no-prototype-builtins: off
  no-regex-spaces: error
  no-sparse-arrays: error
  no-template-curly-in-string: off
  no-unexpected-multiline: error
  no-unreachable: error
  no-unsafe-finally: off
  no-unsafe-negation: off
  use-isnan: error
  valid-jsdoc: off
  valid-typeof: error

  # Best Practices
  accessor-pairs: error
  array-callback-return: off
  block-scoped-var: off
  class-methods-use-this: off
  complexity:
    - error
    - 8
  consistent-return: off
  curly: off
  default-case: off
  dot-location: off
  dot-notation: off
  eqeqeq: error
  guard-for-in: error
  no-alert: error
  no-caller: error
  no-case-declarations: error
  no-div-regex: error
  no-else-return: off
  no-empty-function: off
  no-empty-pattern: error
  no-eq-null: error
  no-eval: error
  no-extend-native: error
  no-extra-bind: error
  no-extra-label: off
  no-fallthrough: error
  no-floating-decimal: off
  no-global-assign: off
  no-implicit-coercion: off
  no-implied-eval: error
  no-invalid-this: off
  no-iterator: error
  no-labels:
    - error
    - allowLoop: true
      allowSwitch: true
  no-lone-blocks: error
  no-loop-func: error
  no-magic-number: off
  no-multi-spaces: off
  no-multi-str: off
  no-native-reassign: error
  no-new-func: error
  no-new-wrappers: error
  no-new: error
  no-octal-escape: error
  no-octal: error
  no-param-reassign: off
  no-proto: error
  no-redeclare: error
  no-restricted-properties: off
  no-return-assign: error
  no-return-await: off
  no-script-url: error
  no-self-assign: off
  no-self-compare: error
  no-sequences: off
  no-throw-literal: off
  no-unmodified-loop-condition: off
  no-unused-expressions: error
  no-unused-labels: off
  no-useless-call: error
  no-useless-concat: error
  no-useless-escape: off
  no-useless-return: off
  no-void: error
  no-warning-comments: off
  no-with: error
  prefer-promise-reject-errors: off
  radix: error
  require-await: off
  vars-on-top: off
  wrap-iife: error
  yoda: off

  # Strict
  strict: off

  # Variables
  init-declarations: off
  no-catch-shadow: error
  no-delete-var: error
  no-label-var: error
  no-restricted-globals: off
  no-shadow-restricted-names: error
  no-shadow: off
  no-undef-init: error
  no-undef: off
  no-undefined: off
  no-unused-vars: off
  no-use-before-define: off

  # Node.js and CommonJS
  callback-return: error
  global-require: error
  handle-callback-err: error
  no-mixed-requires: off
  no-new-require: off
  no-path-concat: error
  no-process-env: off
  no-process-exit: error
  no-restricted-modules: off
  no-sync: off

  # Stylistic Issues
  array-bracket-newline: [ error, consistent ]
  array-bracket-spacing: [ error, always ]
  block-spacing: off
  brace-style:
    - error
    - stroustrup
    - allowSingleLine: true
  camelcase:
    - error
    - properties: always
  capitalized-comments: off
  comma-dangle: error
  comma-spacing: error
  comma-style: error
  computed-property-spacing: error
  consistent-this: off
  eol-last: warn
  func-call-spacing: error
  func-name-matching: off
  func-names: off
  func-style: off
  id-length: off
  id-match: off
  implicit-arrow-linebreak: error
  indent:
    - error
    - 2
    - MemberExpression: 0
      FunctionDeclaration:
        parameters: first
  jsx-quotes: off
  key-spacing: error
  keyword-spacing: error
  line-comment-position: off
  linebreak-style: error
  lines-around-comment: off
  lines-around-directive: off
  lines-between-class-members: error
  max-depth: [ error, 4 ]
  # This one would parse the complete file, and raises a lot of error with html templates
  max-len: off
  max-nested-callbacks: error
  max-params: [ error, 6 ]
  max-statements-per-line:
    - error
    - max: 2
  max-statements:
    - error
    - max: 30
  multiline-ternary: off
  new-cap:
    - error
    - capIsNew: false
  new-parens: error
  newline-per-chained-call: off
  no-array-constructor: error
  no-bitwise: off
  no-continue: off
  no-inline-comments: off
  no-lonely-if: warn
  no-mixed-operators: warn
  no-mixed-spaces-and-tabs: error
  no-multi-assign: off
  no-multiple-empty-lines: warn
  no-negated-condition: off
  no-nested-ternary: error
  no-new-object: error
  no-plusplus: off
  no-restricted-syntax: off
  no-spaced-func: off
  no-tabs: warn
  no-ternary: off
  no-trailing-spaces: warn
  no-underscore-dangle: off
  no-unneeded-ternary: off
  no-whitespace-before-property: error
  object-curly-newline: off
  object-curly-spacing: [ error, always ]
  object-property-newline: off
  one-var-declaration-per-line: off
  one-var: off
  operator-assignment: off
  operator-linebreak: off
  padded-blocks: off
  quote-props: off
  quotes: off
  require-jsdoc: off
  semi: error
  semi-spacing: error
  semi-style: error
  sort-keys: off
  sort-vars: off
  space-before-blocks: error
  space-before-function-paren:
    - error
    - anonymous: never
      named: never
      asyncArrow: always
  space-in-parens: error
  space-infix-ops: error
  space-unary-ops:
    - error
    - words: true
      nonwords: false
  spaced-comment:
    - error
    - always
    - exceptions: [ '@ts-check' ]
      markers: [ ':', '::', '*' ]
  switch-colon-spacing: error
  template-tag-spacing: off
  unicode-bom: error
  wrap-regex: off

  # ECMAScript 6
  arrow-body-style: off
  arrow-parens: error
  arrow-spacing: error
  constructor-super: error
  generator-star-spacing: warn
  no-class-assign: error
  no-confusing-arrow: warn
  no-const-assign: error
  no-dupe-class-members: error
  no-duplicate-imports: error
  no-new-symbol: error
  no-restricted-imports: off
  no-this-before-super: error
  no-useless-computed-key: error
  no-useless-constructor: error
  no-useless-rename: warn
  no-var: off
  object-shorthand: off
  prefer-arrow-callback: off
  prefer-const: warn
  prefer-destructuring: off
  prefer-numeric-literals: warn
  prefer-rest-params: warn
  prefer-reflect: off
  prefer-spread: warn
  prefer-template: off
  require-yield: warn
  rest-spread-spacing: error
  sort-imports:
    - warn
    - ignoreDeclarationSort: true
  symbol-description: error
  template-curly-spacing: off
  yield-star-spacing: error
