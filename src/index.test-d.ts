import { expectType } from 'tsd';

import KnexServer from '.';
import knex from 'knex';
import express from 'express';

const config: KnexServer.Options = {
    client: 'sqlite3',
    useNullAsDefault: true,
    connection: { filename: 'db.sql' },
    definition: {
        info: {
        title: 'My DB',
        version: '1.0.0' }
    }
};

const desc: mrest.DatabaseDescription = {
  runs: {
    table: "RUNS",
    key: "RUN_ID",
    // columns renaming
    mapping: {
      RUN_ID: "id",
      RUN_START: "start",
      RUN_STOP: "stop",
      RUN_TITLE: "title",
      RUN_DESCRIPTION: "description",
      RUN_RUN_NUMBER: "runNumber",
    },
    strict: true,
    rw: true,
    related: {
      documents: {
        path: "documents",
        join: {
          table: "REL_DOCS_RUNS",
          key: "RUN_ID",
          relKey: "DOC_ID"
        }
      },
      comments: {
        path: "comments",
        key: "runId"
      },
    }
  },
};

const info: mrest.KnexRESTSwagger.Info = {
    // routed db path
    basePath: '/db',
    tags: { // documentation tags (group) descriptions
        "/db/documents": "experiment's runs' documents",
        "/db/runs": "experiment's runs database"
    },
    paths: {
        "/db/documents": {
            "get.responses.200.examples.[application/json]": [ {
                "id": 86,
                "title": "Test2.pdf",
                "name": "Test2.pdf",
                "size": 81255,
                "document": null
            }]
        }
    }
};


(async function() {
    const server = new KnexServer(config, desc, info); 

    const app = express();
    server.register(app);

    app.get('/', KnexServer.router(config, desc, info));

    expectType<mrest.KnexREST>(server.tables['runs']);
    expectType<mrest.KnexTransactionManager>(server.knexTxMgr);

    const service = new KnexServer.KnexREST(server.knexTxMgr, 'test', desc.runs);
    service.register(app);
    expectType<string>(service.table);
    expectType<string>(service.path);
    expectType<mrest.KnexREST.Options>(service.options);

    expectType<express.RequestHandler>(KnexServer.ExpressHelpers.wrap((req, res) => 42));
}());