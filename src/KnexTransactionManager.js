const { isNil, map } = require('lodash');
const Timer = require('./Timer');

// @ts-check
const
  debug = require('debug')('rest:db:trx'),
  bodyParser = require('body-parser'),
  crypto = require('crypto'),
  { HTTPError, wrap } = require('./ExpressHelpers'),
  { get } = require('./lodash-ext');

/**
 * @typedef {import('knex').Knex} Knex
 * @typedef {import('knex').Knex.Transaction} Transaction
 * @typedef {import('knex').Knex.TransactionProvider} TransactionProvider
 * @typedef {import('express').Router} Router
 * @typedef {import('express').Response} Response
 * @typedef {import('express').Request} Request
 *
 * @typedef {{
 *   provider: TransactionProvider,
 *   expiry: Timer
 * }} TransactionInfo
 */

const Action = {
  COMMIT: 0,
  ROLLBACK: 1
};

const ActionResult = {
  DONE: 0,
  ERROR: 1,
  TRX_NOT_EXIST: 2
};

class KnexTransactionManager {
  /**
   * @param {Knex} knex Knex principal DB connection object
   * @param {boolean} enabled Knex principal DB connection object
   */
  constructor(knex, enabled = true) {
    this.knex = knex;
    this.enabled = enabled;
    /** @type {{ [uuid: string]: TransactionInfo }} */
    this.transactions = {};
  }

  /**
   * @param {Router} app
   * @return {KnexTransactionManager}
   */
  register(app) {
    if (!this.enabled) {
      debug("KnexTransactionManager is disabled. No Api registrations.");
      return this;
    }
    var path = '/db/transaction';
    app.get(path, wrap(this.listTransactions.bind(this)));
    app.post(path, KnexTransactionManager.JsonParser,
      wrap(this.createTransaction.bind(this)));
    app.post(path + '/:uuid', KnexTransactionManager.JsonParser,
      wrap(this.commitTransaction.bind(this)));
    app.delete(path + '/:uuid', KnexTransactionManager.JsonParser,
      wrap(this.cancelTransaction.bind(this)));
    return this;
  }

  async release() {
    // Stop/Delete all the expiry Timers and rollback
    // eslint-disable-next-line guard-for-in
    for (const uuid in this.transactions) {
      await this._doAction(uuid, Action.ROLLBACK);
    }
    this.transactions = {};
    debug('KnexTransactionManager released.');
  }

  /**
   * @swagger
   * /db/transaction:
   *   get:
   *     tags: ['db']
   *     summary: list all active db transaction
   *     description: list currently active db transaction with time remaining before expiring
   *     produces: application/json
   *     parameters:
   *     responses:
   *       200:
   *         description: the list of transactions information
   *         examples:
   *           application/json:
   *             - transactionId: 528a1d2a-541a-41b2-ba51-714b03de3de8
   *               expiry: 9005
   * @param {Request} req
   * @param {Response} res
   */
  async listTransactions(req, res) {  /* jshint unused:false */
    return map(this.transactions, (trx, uuid) => {
      return { transactionId: uuid, expiry: trx.expiry.getTimeLeft() };
    });
  }

  /**
   * @swagger
   * /db/transaction:
   *   post:
   *     tags: ['db']
   *     summary: create a new db transaction
   *     description: create a new db transaction that can be committed or rollbacked via api
   *     produces: application/json
   *     parameters:
   *     responses:
   *       200:
   *         description: the transaction information
   *         examples:
   *           application/json:
   *             transactionId: 528a1d2a-541a-41b2-ba51-714b03de3de8
   * @param {Request} req
   * @param {Response} res
   */
  async createTransaction(req, res) {  /* jshint unused:false */
    const uuid = crypto.randomUUID();
    /**
     * transactionProvider() create a reusable transaction instance,
     * but it won't start it until it is used.
     * It will start transaction after being called for the first time.
     * Consecutive calls of trx() will return always the same transaction
     */
    const provider = this.knex.transactionProvider();
    const expiryTimeMs = get(req.body, 'expiryTimeMs', KnexTransactionManager.TRX_EXPIRY_SECONDS * 1000);
    this.transactions[uuid] = {
      provider,
      expiry: new Timer(this._doAction.bind(this, uuid, Action.ROLLBACK), expiryTimeMs)
    };
    debug(`Transaction:${uuid} created`);
    return { transactionId: uuid };
  }

  /**
   * @swagger
   * /db/transaction/{uuid}:
   *   post:
   *     tags: ['db']
   *     summary: commit a specific transaction
   *     description: if exists, it will commit transaction {uuid}
   *     produces: application/json
   *     parameters:
   *     responses:
   *       200:
   *         description: transaction {uuid} committed
   *       400:
   *         description: transaction {uuid} not found
   * @param {Request} req
   * @param {Response} res
   */
  async commitTransaction(req, res) {  /* jshint unused:false */
    const uuid = get(req.params, 'uuid');
    const ret = await this._doAction(uuid, Action.COMMIT);
    if (ret === ActionResult.TRX_NOT_EXIST) {
      throw new HTTPError(400, 'transaction not found');
    }
    return true;
  }

  /**
   * @swagger
   * /db/transaction/{uuid}:
   *   delete:
   *     tags: ['db']
   *     summary: rollback a specific transaction
   *     description: if exists, it will rollback transaction {uuid}
   *     produces: application/json
   *     parameters:
   *     responses:
   *       200:
   *         description: transaction {uuid} rollbacked
   *       400:
   *         description: transaction {uuid} not found
   * @param {Request} req
   * @param {Response} res
   */
  async cancelTransaction(req, res) {  /* jshint unused:false */
    const uuid = get(req.params, 'uuid');
    const ret = await this._doAction(uuid, Action.ROLLBACK);
    if (ret === ActionResult.TRX_NOT_EXIST) {
      throw new HTTPError(400, 'transaction not found');
    }
    return true;
  }

  /**
   * @brief Rollback or Commit the transaction if is started
   * @param {string} uuid
   * @param {number} action
   * @returns {Promise<number>}
   */
  async _doAction(uuid, action) {
    const trx = await this._getTrx(uuid);
    if (isNil(trx)) { return ActionResult.TRX_NOT_EXIST; }
    if (trx.isCompleted()) { return ActionResult.DONE; }
    if (action === Action.COMMIT) {
      await trx.commit();
      debug(`Transaction:${uuid} committed`);
    }
    else {
      await trx.rollback();
      debug(`Transaction:${uuid} rollbacked`);
    }
    this.transactions[uuid].expiry.pause();
    delete this.transactions[uuid];
    return ActionResult.DONE;
  }

  /**
   * @param {string?=} uuid
   * @returns {Promise<Transaction> | undefined}
   */
  _getTrx(uuid) {
    if (isNil(uuid)) { return; }
    return this.transactions?.[uuid]?.provider();
  }

  /**
   * @brief Api that returns one of (in this order):
   *  1) a transaction from the provider if uuid is present in the req as parameter (?transactionId=<uuid>)
   *  2) transaction attached to req (knexTransaction attribute)
   *  3) the original knex
   * @param {Request} req
   * @returns {Promise<Transaction | Knex>}
   */
  async getKnex(req) {
    // If tx manager is disable, return the original connection
    if (!this.enabled) { return this.knex; }
    // Furst check if a transaction created via rest api exists
    const uuid = get(req, 'query.transactionId');
    const trx = await this._getTrx(uuid);
    if (!isNil(trx)) { return trx; }
    else if (isNil(trx) && !isNil(uuid)) {
      // In case rest api tx doen't exist and an uuid is provided, raise an error
      throw new HTTPError(400, 'Invalid transaction id: ' + uuid);
    }
    // return, if exist, the transaction binded to req, otherwise the default connection
    return get(req, 'knexTransaction', this.knex);
  }
}

KnexTransactionManager.JsonParser = bodyParser.json({ strict: false });
KnexTransactionManager.TRX_EXPIRY_SECONDS = 10;

module.exports = KnexTransactionManager;
