// @ts-check
const
  debug = require('debug')('rest:helpers'),
  { get, noop } = require('lodash');

/**
 * @typedef {import('express').Response} Response
 * @typedef {import('express').Request} Request
 * @typedef {import('express').RequestHandler} RequestHandler
 */

class HTTPError extends Error {
  /**
   * @param {number} status
   * @param {string=} message
   */
  constructor(status, message) {
    super(message);
    this.status = status;

    switch (Math.floor(status / 100)) {
    case 4:
      this.code = 'HTTPClientError';
      break;
    case 5:
      this.code = 'HTTPServerError';
      break;
    default:
      this.code = 'HTTPError';
      break;
    }
  }
}

/**
 * @param {Response} res
 * @param {any} ret
 * @return {any}
 */
function jsonRet(res, ret) {
  if (res.writableEnded) { return; }
  return res.json(ret ?? null);
}

/**
 * @param {Response} res
 * @param {Error} ret
 */
function errorRet(res, ret) {
  debug('error: %o', ret);
  try {
    res
    .status(get(ret, 'status', 500))
    .send(get(ret, 'message', ret));
  }
  catch (/** @type {any} */ err) {
    console.error(`Failed to send error: ${err.message}`);
  }
}

/**
 * @param {(...args: any[]) => any} fun
 * @return {RequestHandler}
 */
function promiseWrapper(fun) {
  return function(req, res, next) {
    try {
      var ret = fun(req, res, next);
      if (get(ret, 'then', false)) {
        return ret.then(
          jsonRet.bind(null, res),
          errorRet.bind(null, res))
        .catch(noop);
      }
      jsonRet(res, ret);
      return ret;
    }
    catch (/** @type {any} */ err) {
      errorRet(res, err);
    }
  };
}

module.exports = {
  HTTPError,
  wrap: promiseWrapper
};
