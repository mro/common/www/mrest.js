/// <reference types="node" />

import { Knex } from 'knex';
import { NextFunction, Request, Response, Router, RequestHandler } from 'express';

export = mrest;
export as namespace mrest;

declare namespace mrest {
    type BaseValue = string|number|RegExp|boolean

    interface Op {
      $eq?: BaseValue;
      $lt?: number;
      $lte?: number;
      $gt?: number;
      $gte?: number;
      $in?: Array<BaseValue>;
      $nin?: Array<BaseValue>;
      $ne?: BaseValue;
      $like?: string;
      $options?: any;
      $not?: Op|Query|BaseValue;
      $or?: Array<Query>;
      $and?: Array<Query>;
      [key: string]: any;
    }
  
    type Item = { [key: string]: any }
  
    interface Query {
      // had to use any to overlap or/and/not declarations
      [key: string]: string|number|RegExp|Op|any;
      $or?: Array<Query>;
      $and?: Array<Query>;
      $not?: Query;
    }

    interface DirectRelation {
        path: string;
        key: string;
    }

    interface JoinTableRelation {
        path: string;
        key?: string;
        join: {
            table: string;
            key: string;
            relKey: string;
        }
    }

    type Relation = DirectRelation | JoinTableRelation

    interface TableDescription {
        table: string,
        key?: string,
        path?: string,
        mapping?: { [column: string]: string },
        streams?: { [column: string]: string },
        strict?: boolean,
        rw?: boolean,
        related?: { [name: string]: Relation }
    }

    interface DatabaseDescription {
        [path: string]: TableDescription
    }

    class KnexREST {
        constructor(knexTxMgr: KnexTransactionManager, table: string, options?: Partial<TableDescription>);
        options: KnexREST.Options;
        table: string;
        path: string;
        knexTxMgr: KnexTransactionManager;

        search(req: Request, res: Response): Promise<any[]>;
        explain(req: Request, res: Response): Promise<string>;
        get(req: Request, res: Response): Promise<any[]>;
        delete(req: Request, res: Response): Promise<void>;
        insert(req: Request, res: Response): Promise<void>;
        update(req: Request, res: Response): Promise<void>;
        listRelated(req: Request, res: Response): Promise<string[]>;
        count(req: Request, res: Response): Promise<number>;
        
        related(path: string, key: string, router: Router, req: Request, res: Response): Promise<void>;
        relatedJoin(path: string, joinInfo: JoinTableRelation, router: Router, req: Request, res: Response): Promise<void>;
        register(app: Router): KnexREST;
        static list(req: Request, res: Response): Promise<string[]>;
        static register(app: Router): void;
    }

    namespace KnexREST {
        interface Options extends Partial<TableDescription> {
            revmapping?: { [name: string]: string },
            revstreams?: { [name: string]: string }
        }

        const tables: string[];
        const JsonParser: NextFunction;
    }

    class KnexRESTSwagger {
        constructor(options: KnexRESTSwagger.Options, extraDoc?: KnexRESTSwagger.Info);
        doc: any;
        extra: KnexRESTSwagger.Info|null|undefined;

        swagTemplate(service: KnexREST, template: string, path: string, omit: string[]|null|undefined): any;
        register(router: Router): void;

        swag(service: KnexREST): any;
    }

    namespace KnexRESTSwagger {
        interface Options {
            definition: { info: { title: string, version: string } }
        }

        interface Info {
            basePath?: string;
            tags?: { [path: string]: string };
            paths?: { [path: string]: any }
        }
    }

    class KnexServer {
        constructor(config: KnexServer.Options, dbInfo: DatabaseDescription, swaggerInfo: KnexRESTSwagger.Info);
        config: KnexServer.Options;
        dbInfo: DatabaseDescription;
        swaggerInfo: KnexRESTSwagger.Info;
        tables: { [path: string]: KnexREST };
        knex: Knex;
        knexTxMgr: KnexTransactionManager;

        register(router: Router): Router;
        release(): void;
        static router(config: KnexServer.Options, dbInfo: DatabaseDescription, swaggerInfo: KnexRESTSwagger.Info): Router;
    }

    class Timer {
        constructor(callback: () => void, delay: number);

        start(): void;
        pause(): void;
        getTimeLeft(): number;
        isRunning(): boolean;
    }
    
    namespace KnexTransactionManager {
        interface TransactionInfo {
            provider: Knex.TransactionProvider,
            expiry: Timer
        }
    }

    class KnexTransactionManager {
        constructor(knex: Knex, disabled: boolean);
        knex: Knex;
        transactions: { [uuid: string]: KnexTransactionManager.TransactionInfo }

        register(app: Router): KnexTransactionManager;
        createTransaction(req: Request, res: Response): Promise<any>;
        commitTransaction(req: Request, res: Response): Promise<boolean>;
        cancelTransaction(req: Request, res: Response): Promise<boolean>;
        getKnex(req: Request): Promise<Knex.Transaction | Knex>;
    }

    namespace ExpressHelpers {
      class HTTPError extends Error {
        constructor(status: number, message: string);
      }
      function wrap(fun: RequestHandler): RequestHandler;
    }

    namespace KnexServer {
        const KnexREST: typeof mrest.KnexREST;
        type Options = KnexRESTSwagger.Options & Knex.Config & {
            knex?: Knex;
            transactions?: boolean;
        };

        const ExpressHelpers: typeof mrest.ExpressHelpers;
    }
}
