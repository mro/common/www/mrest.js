// @ts-check
const
  { forEach, get } = require('lodash'),
  knex = require('knex').default,
  express = require('express'),
  KnexREST = require('./KnexREST'),
  KnexRestSwagger = require('./KnexRESTSwagger'),
  KnexTransactionManager = require('./KnexTransactionManager'),
  ExpressHelpers = require('./ExpressHelpers');

/**
 * @typedef {import('knex').Knex} Knex
 * @typedef {import('express').Router} Router
 */

class KnexServer {
  /**
   * @param {mrest.KnexServer.Options} config
   * @param {mrest.DatabaseDescription} dbInfo
   * @param {mrest.KnexRESTSwagger.Info} swaggerInfo
   */
  constructor(config, dbInfo, swaggerInfo) {
    this.config = config;
    this.swaggerInfo = swaggerInfo;
    this.dbInfo = dbInfo;
    /** @type {Knex} */
    this.knex = (config && config.knex) ? config.knex : knex(config);
    this.knexTxMgr = new KnexTransactionManager(this.knex, get(config, 'transactions'));
    /** @type {{ [path: string]: KnexREST }} */
    this.tables = {};
  }

  /**
   * @param {Router} router
   * @return {Router}
   */
  register(router) {
    const dbSwagger = new KnexRestSwagger(this.config, this.swaggerInfo);

    forEach(this.dbInfo, (desc, path) => {
      const db = new KnexREST(this.knexTxMgr, path, desc);
      db.register(router);
      this.tables[path] = db;
      dbSwagger.swag(db);
    });

    KnexREST.register(router);
    dbSwagger.register(router);
    this.knexTxMgr.register(router);

    return router;
  }

  release() {
    if (this.knex && !this.config.knex) {
      this.knex.destroy();
    }
    // @ts-ignore object is invalid now
    this.knex = null;
    this.knexTxMgr.release();
  }

  /**
   * @param {mrest.KnexServer.Options} config
   * @param {mrest.DatabaseDescription} dbInfo
   * @param {mrest.KnexRESTSwagger.Info} swaggerInfo
   * @return {Router}
   */
  static router(config, dbInfo, swaggerInfo) {
    const server = new KnexServer(config, dbInfo, swaggerInfo);
    return server.register(express.Router());
  }
}
KnexServer.KnexREST = KnexREST;
KnexServer.ExpressHelpers = ExpressHelpers;

module.exports = KnexServer;
