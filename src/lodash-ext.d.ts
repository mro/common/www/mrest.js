
// extending lodash
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import _ from 'lodash';

// eslint-disable-next-line no-redeclare
declare module "lodash" {
  // eslint-disable-next-line no-redeclare
  interface LoDashStatic {
    push<T>(array: T[]|null|undefined, value: T): T[];
    bind0<Ret=any>(func: (...args: any[]) => Ret, thisArg: any, ...args: any[]): () => Ret;
    mbind<Ret=any>(object: any, methodName: string, ...args: any[]): (...args: any[]) => Ret;
    fbind: typeof _.partial;
    fbind0<Ret=any>(func: (...args: any[]) => Ret, ...args: any[]): () => Ret;
    mbind0<Ret=any>(object: any, methodName: string, ...args: any[]): () => Ret;
  }
}

export = _