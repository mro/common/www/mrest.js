class Timer {
  /**
   * @brief This class implement a Timer with time remaining calculation
   * @param {() => void} callback
   * @param {number} delay
   */
  constructor(callback, delay) {
    this.callback = callback;
    this.delay = delay;
    /** @type {NodeJS.Timeout} */ // eslint-disable-next-line no-unused-expressions
    this.id;
    this.timeStarted = 0;
    this.timeRemaining = delay;
    this.running = false;

    this.start();
  }

  start() {
    this.running = true;
    this.timeStarted = new Date().getTime();
    this.id = setTimeout(this.callback, this.timeRemaining);
  }

  pause() {
    this.running = false;
    clearTimeout(this.id);
    this.timeRemaining -= (new Date().getTime() - this.timeStarted);
  }

  /**
   * @returns {number}
   */
  getTimeLeft() {
    if (this.running) {
      this.pause();
      this.start();
    }
    return this.timeRemaining;
  }

  /**
   * @returns {boolean}
   */
  isRunning() {
    return this.running;
  }
}

module.exports = Timer;
