// @ts-check
const
  { get, wrap } = require('lodash'),
  debug = require('debug')('rest:db:oracle'),
  { makeDeferred } = require('@cern/prom'),
  { HTTPError } = require('../ExpressHelpers');

/** @type {any} */
var oracledb;
try {
  /* eslint-disable global-require */ // @ts-ignore
  oracledb = require('oracledb');
}
catch (e) {
  debug('no oracle support');
}

/**
 * @typedef {import('knex').Knex} Knex
 * @typedef {import('express').Response} Response
 * @typedef {import('express').Request} Request
 */

/**
 * @brief stream content from oracle database
 * @param  {Knex} knex
 * @param  {string} table
 * @param  {string} dataColumn
 * @param  {string} idColumn
 * @param  {string} id
 * @param  {Response} res
 * @return {Promise<void>}
 */
async function streamGet(knex, table, dataColumn, idColumn, id, res) {
  const conn = await knex.client.acquireConnection();
  debug('direct file stream download');

  return conn.execute(
    `SELECT "${dataColumn}" FROM "${table}" WHERE ${idColumn} = :id`,
    { id })
  .then(async (/** @type {any} */ ret) => {
    const lob = get(ret, [ 'rows', 0, 0 ]);
    if (!lob) {
      throw new HTTPError(404, 'No such item: ' + id);
    }

    const defer = makeDeferred();
    lob.once('end', () => res.end());
    lob.once('close', () => defer.resolve(undefined));
    lob.once('error', (/** @type {Error} */ err) => defer.reject(err));
    res.writeHead(200, { 'Content-Type': 'application/octet-stream' });
    lob.pipe(res);
    await defer.promise;
  })
  .finally(() => knex.client.releaseConnection(conn));
}

/**
 * @brief stream content to oracle database
 * @param  {Knex} knex
 * @param  {string} table
 * @param  {string} dataColumn
 * @param  {string} idColumn
 * @param  {string} id
 * @param  {Request} req
 * @return {Promise<void>}
 */
async function streamPut(knex, table, dataColumn, idColumn, id, req) {
  const conn = await knex.client.acquireConnection();
  debug('direct file stream upload');
  return conn.execute(
    `UPDATE "${table}" SET "${dataColumn}" = EMPTY_BLOB() WHERE ${idColumn} = :id RETURNING "${dataColumn}" INTO :lobbv`,
    { id, lobbv: { type: oracledb.BLOB, dir: oracledb.BIND_OUT } },
    { autoCommit: false })
  .then(async (/** @type {any} */ ret) => {
    const lob = get(ret, [ 'outBinds', 'lobbv', 0 ]);
    if (!lob || ret.rowsAffected !== 1) {
      throw new HTTPError(404, 'No such item: ' + id);
    }

    const defer = makeDeferred();
    lob.once('finish', () => conn.commit((/** @type {Error} */ err) => {
      if (err) {
        lob.destroy(err);
      }
      else {
        defer.resolve(undefined);
      }
    }));
    lob.once('error', (/** @type {Error} */ err) => defer.reject(err));
    req.once('error', (/** @type {Error} */ err) => lob.destroy(err));
    req.pipe(lob);
    await defer.promise;
  })
  .finally(() => knex.client.releaseConnection(conn));
}

/*
 * Force `like` queries to always use binary_ai collation
 * @details whereLike exists only in Knex >= 1.0.0
 */
// @ts-ignore
const qc = require('knex/lib/dialects/oracledb/query/oracledb-querycompiler');
qc.prototype.whereBasic = wrap(qc.prototype.whereBasic,
  /** @this {qc} */ function(func, statement) {
    if (statement.operator === 'like') {
      return this.whereLike(statement);
    }
    return func.call(this, statement);
  });
qc.prototype.whereLike = wrap(qc.prototype.whereLike,
  /** @this {qc} */ function(func, statement) {
    return func.call(this, statement) + ' COLLATE binary_ai';
  });

module.exports = { streamGet, streamPut };
