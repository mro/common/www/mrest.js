// @ts-check
const
  debug = require('debug')('rest:db'),
  { set, get, noop, reduce, pick, invert, has, push, includes, map, mapKeys,
    isNil, isArray, keys, first, forEach, split, isString,
    isEmpty, isPlainObject, toLower } = require('./lodash-ext'),
  bodyParser = require('body-parser'),
  { makeDeferred } = require('@cern/prom'),
  { HTTPError, wrap } = require('./ExpressHelpers'),
  oracle = require('./drivers/oracle');

/**
 * @typedef {import('knex').Knex.QueryBuilder} QueryBuilder
 * @typedef {import('knex').Knex} Knex
 * @typedef {import('express').Response} Response
 * @typedef {import('express').Request} Request
 * @typedef {import('express').RequestHandler} RequestHandler
 * @typedef {import('express').Application} App
 * @typedef {import('express').Router} Router
 * @typedef {import('./KnexTransactionManager')} KnexTransactionManager
 */

/**
 * @swagger
 * parameters:
 *   dbTable:
 *     name: table
 *     in: path
 *     required: true
 *     description: requested table
 *     schema: {}
 *   dbItem:
 *     name: id
 *     description: the item id
 *     in: path
 *     required: true
 *     schema: {}
 *   dbRelation:
 *     name: relation
 *     description: related items table name
 *     in: path
 *     required: true
 *     schema: {}
 *   searchMax:
 *     name: max
 *     description: max items in result
 *     in: query
 *     required: false
 *     type: integer
 *     default: 100
 *   searchOffset:
 *     name: offset
 *     description: result's offset
 *     in: query
 *     required: false
 *     type: integer
 *     default: 0
 *   searchFilter:
 *     name: filter
 *     collectionFormat: multi
 *     description: filtering rules syntax
 *     example: >
 *       <br/>
 *       This syntax is recommanded for basic filters
 *         - filter=*columnName*:*value*
 *         - filter[*columnName*]=*value*
 *         - filter[*columnName*][gte]=*value*
 *
 *       supported operators:
 *         - $ne: not equal
 *         - $gt, $gte: greater (or equal) than
 *         - $lt, $lte: less than
 *     in: query
 *     required: false
 *   searchQuery:
 *     name: query
 *     description: 'MongoDB query'
 *     example: '{"*columnName*": { "$gte": *value* } }'
 *     type: object
 *     in: query
 *     required: false
 *   searchQueryHeader:
 *     name: X-MRest-Query
 *     description: 'MongoDB query'
 *     example: '{"*columnName*": { "$gte": *value* } }'
 *     schema:
 *       type: string
 *       format: json
 *     in: header
 *     required: false
 *   searchSort:
 *     name: sort
 *     description: 'results order'
 *     example: >
 *       :
 *         - sort=*columnName* (defaults to asc)
 *         - sort=*columnName*:asc
 *         - sort=*columnName*:desc
 *     in: query
 *     required: false
 */

const compOp = {
  $eq: '=', $ne: '<>',
  $gt: '>', $gte: '>=',
  $lt: '<', $lte: '<=',
  $in: 'in', $nin: 'not in', $like: 'like'
};

const nullOp = {
  $eq: 'Null', $ne: 'NotNull',
  $gt: 'NotNull', $gte: null,
  $lt: 'NotNull', $lte: null,
  $in: null, $nin: null,
  $like: 'Null', $not: 'NotNull'
};

const arrayOp = {
  /** @type {(k: KnexREST, parentKey: string|null, query: QueryBuilder, values: mrest.Query[]) => QueryBuilder} */
  $or: (k, parentKey, query, values) => query.where(
    (builder) => reduce(values, (b, v) => k._parseQuery(b, parentKey, v, true), builder)),
  /** @type {(k: KnexREST, parentKey: string|null, query: QueryBuilder, values: mrest.Query[]) => QueryBuilder} */
  $and: (k, parentKey, query, values) => query.where(
    (builder) => reduce(values, (b, v) => k._parseQuery(b, parentKey, v, false), builder)),
  /** @type {(k: KnexREST, parentKey: string|null, query: QueryBuilder, values: mrest.Query[]) => QueryBuilder} */
  $not: (k, parentKey, query, value) => query.whereNot(function() {
    return k._parseQuery(this, parentKey, value, false);
  })
};

/**
 * KnexREST SQL REST wrapper
 */
class KnexREST {
  /**
   * KnexREST constructor
   * @param {KnexTransactionManager} knexTxMgr    Knex DB connection object
   * @param {string} table   Table to wrap (also used as default path)
   * @param {Partial<mrest.TableDescription>} options Wrapper options (see details)
   *
   * @details Available options:
   * - key: primary key column name
   * - path: the HTTP path for this database
   * - table: override the table argument (using it only for path)
   * - mapping: column public name mapping (column: public_name)
   * - strict: check that all filter/sort criterias match a mapping
   * - related: related tables description
   */
  constructor(knexTxMgr, table, options) {
    this.knexTxMgr = knexTxMgr;
    /** @type {mrest.KnexREST.Options} */
    this.options = pick(options, [ 'mapping', 'path', 'table', 'key',
      'strict', 'related', 'rw', 'streams' ]);
    this.table = get(this.options, 'table', table);
    this.path = get(this.options, 'path', table);

    if (has(this.options, 'mapping')) {
      // @ts-ignore checked above
      set(this.options, 'revmapping', invert(this.options.mapping));

      /* default key mapping */
      if (this.options.key && !has(this.options.mapping, this.options.key)) {
        // @ts-ignore checked above
        set(this.options.revmapping, this.options.key, this.options.key);
        // @ts-ignore checked above
        set(this.options.mapping, this.options.key, this.options.key);
      }
    }
    if (has(this.options, 'streams')) {
      // @ts-ignore checked above
      set(this.options, 'revstreams', invert(this.options.streams));
    }

    if (!includes(KnexREST.tables, this.path)) {
      // $FlowIgnore: provided by lodash-ext
      KnexREST.tables = push(KnexREST.tables, this.path);
    }
    debug('DB wrapper for:', this.path);
  }

  /**
   * @swagger
   * /db/{table}:
   *   get:
   *     tags: ['db']
   *     summary: retrieve or search for items
   *     description: |+
   *       retrieve or search for items in a database<br/><br/>
   *       **Note:** both *sort* and *filter* can be repeated in query string or multi-valued using csv (comma) separator:<br/>
   *         - filter=*columnName*:*value*&filter=*columnName2*:*value2*<br/>
   *         - sort=*columnName*,*columnName2*:asc<br/>
   *     produces: application/json
   *     parameters:
   *       - $ref: '#/parameters/dbTable'
   *       - $ref: '#/parameters/searchMax'
   *       - $ref: '#/parameters/searchOffset'
   *       - $ref: '#/parameters/searchFilter'
   *       - $ref: '#/parameters/searchQuery'
   *       - $ref: '#/parameters/searchQueryHeader'
   *       - $ref: '#/parameters/searchSort'
   *     responses:
   *       200:
   *         description: items from {table}
   *         examples:
   *           application/json:
   *             - id: 42
   *               name: sample
   *               color: blue
   *             - id: 44
   *               name: test
   *               color: yellow
   * @param {Request} req
   * @param {Response} res
   * @return {Promise<any[]>}
   */
  async search(req, res) { /* jshint unused:false */
    this._noCache(res);
    const knex = await this.knexTxMgr.getKnex(req);
    var query = this._buildQuery(req, knex(this.table).select());
    query = this._parseSort(query, get(req, 'query.sort'));

    if (this.options.strict && this.options.revmapping) {
      query = query.column(this.options.revmapping);
    }

    return query
    .limit(get(req, 'query.max', 100))
    .offset(get(req, 'query.offset', 0))
    .then(
      (ret) => {
        if (!this.options.strict && this.options.mapping) {
          return map/*:: <{...},{...}> */(ret, (item) => mapKeys(item, (value, key) => this._fromDB(key)));
        }
        return ret;
      }
    );
  }

  /**
   * @swagger
   * /db/{table}/explain:
   *   get:
   *     tags: ['db']
   *     summary: give details about the internal request
   *     parameters:
   *       - $ref: '#/parameters/dbTable'
   *       - $ref: '#/parameters/searchMax'
   *       - $ref: '#/parameters/searchOffset'
   *       - $ref: '#/parameters/searchQuery'
   *       - $ref: '#/parameters/searchQueryHeader'
   *       - $ref: '#/parameters/searchSort'
   *     responses:
   *       200:
   *         description: items from {table}
   *         examples:
   *           application/json:
   *            ""
   * @param {Request} req
   * @param {Response} res
   * @return {Promise<string>}
   */
  async explain(req, res) {
    this._noCache(res);
    const knex = await this.knexTxMgr.getKnex(req);
    var query = this._buildQuery(req, knex(this.table).select());
    query = this._parseSort(query, get(req, 'query.sort'));

    if (this.options.strict && this.options.revmapping) {
      query = query.column(this.options.revmapping);
    }

    return query
    .limit(get(req, 'query.max', 100))
    .offset(get(req, 'query.offset', 0))
    .toString();
  }

  /**
   * @swagger
   * /db/{table}/item/{id}:
   *   get:
   *     tags: ['db']
   *     summary: retrieve a specific item
   *     description: retrieve a specific item
   *     produces: application/json
   *     parameters:
   *       - $ref: '#/parameters/dbTable'
   *       - $ref: '#/parameters/dbItem'
   *     responses:
   *       200:
   *         description: the requested item
   *         examples:
   *           application/json:
   *             id: 42
   *             name: sample
   *             color: blue
   *       404:
   *         description: item does not exist
   * @param {Request} req
   * @param {Response} res
   * @return {Promise<any[]>}
   */
  async get(req, res) { /* jshint unused:false */
    this._noCache(res);
    const knex = await this.knexTxMgr.getKnex(req);
    var query = knex(this.table).first();
    if (this.options.strict && this.options.revmapping) {
      query = query.column(this.options.revmapping);
    }

    const ret = await query // @ts-ignore method not used if key not in options
    .where(this.options.key, get(req.params, 'id'));

    if (isNil(ret)) {
      throw new HTTPError(404);
    }
    else if (!this.options.strict && this.options.mapping) {
      return map/*:: <{...},{...}> */(ret,
        (item) => mapKeys(item, (value, key) => this._fromDB(key)));
    }
    return ret;
  }

  /**
   * @swagger
   * /db/{table}/item/{id}:
   *   delete:
   *     tags: ['db']
   *     summary: remove an item
   *     description: remove an item from the db
   *     parameters:
   *       - $ref: '#/parameters/dbTable'
   *       - $ref: '#/parameters/dbItem'
   *     responses:
   *       200:
   *         description: item was removed
   *       404:
   *         description: item does not exist
   * @param {Request} req
   * @param {Response} res
   * @return {Promise<void>}
   */
  async delete(req, res) { /* jshint unused:false */
    this._noCache(res);
    const knex = await this.knexTxMgr.getKnex(req);
    var query = knex(this.table);

    return query // @ts-ignore method not used when no key
    .where(this.options.key, get(req.params, 'id'))
    .delete();
  }

  /**
   * @swagger
   * /db/{table}:
   *   delete:
   *     tags: ['db']
   *     summary: delete searched items
   *     description: |+
   *       delete items matching criteria from database<br/><br/>
   *       **Note:** *filter* can be repeated in query string or multi-valued using csv (comma) separator:<br/>
   *         - filter=*columnName*:*value*&filter=*columnName2*:*value2*<br/>'
   *     parameters:
   *       - $ref: '#/parameters/dbTable'
   *       - $ref: '#/parameters/searchFilter'
   *       - $ref: '#/parameters/searchQuery'
   *       - $ref: '#/parameters/searchQueryHeader'
   *     responses:
   *       200:
   *        description: items were removed
   * @param  {Request} req
   * @param  {Response} res
   * @return {Promise<void>}
   */
  async deleteAll(req, res) {
    this._noCache(res);
    const knex = await this.knexTxMgr.getKnex(req);
    var query = this._buildQuery(req, knex(this.table));

    return query.delete();
  }

  /**
   * @swagger
   * /db/{table}:
   *   post:
   *     tags: ['db']
   *     summary: insert an item
   *     description: insert item into the db
   *     parameters:
   *       - $ref: '#/parameters/dbTable'
   *       - in: body
   *         name: items or items
   *         description: 'item or array of items to create'
   *         required: true
   *         example: '{}'
   *     responses:
   *       200:
   *         description: the last insert item id
   *         examples:
   *           application/json:
   *             42
   * @param {Request} req
   * @param {Response} res
   * @return {Promise<void>}
   */
  async insert(req, res) { /* jshint unused:false */
    this._noCache(res);
    const knex = await this.knexTxMgr.getKnex(req);
    var query = knex(this.table);
    const data = isArray(req.body) ?
      map(req.body, (i) => mapKeys(i, (v, k) => this._toDB(k))) :
      mapKeys(req.body, (v, k) => this._toDB(k));

    query.insert(data);
    // database engines are inconsistent regarding result of insert,
    // MySQL and sqlite have issues with "returning" statement
    if (this.options.key) {
      if (get(knex, [ 'client', 'dialect' ]) !== 'sqlite3') {
        query.returning(this.options.key);
      }
      // @ts-ignore this.options.key is not undefined
      return query.then(first).then((ret) => get(ret, this.options.key, ret));
    }
    return query.then(noop);
  }

  /**
   * @swagger
   * /db/{table}/item/{id}:
   *   put:
   *     tags: ['db']
   *     summary: modify an item
   *     descritpion: modify an existing item in db
   *     parameters:
   *       - $ref: '#/parameters/dbTable'
   *       - $ref: '#/parameters/dbItem'
   *       - in: body
   *         name: items or items
   *         description: 'item or array of items to create'
   *         required: true
   *         example: '{}'
   *     reponses:
   *       200:
   *         description: item was updated
   * @param {Request} req
   * @param {Response} res
   * @return {Promise<void>}
   */
  async update(req, res) { /* jshint unused:false */
    this._noCache(res);
    const knex = await this.knexTxMgr.getKnex(req);
    var query = knex(this.table);
    const data = mapKeys(req.body, (v, k) => this._toDB(k));

    return query // @ts-ignore method not used when no key
    .where(this.options.key, get(req.params, 'id')) // $FlowIgnore
    .update(data);
  }

  /**
   * @swagger
   * /db/{table}/item/{id}/related/{relation}:
   *   get:
   *     tags: ['db']
   *     summary: retrieve related items
   *     description: |+
   *       Shorthand method to query another table for related items.<br/><br/>
   *       **Note:** the same query parameters than on a direct table query can be used'
   *     produces: application/json
   *     parameters:
   *       - $ref: '#/parameters/dbTable'
   *       - $ref: '#/parameters/dbItem'
   *       - $ref: '#/parameters/dbRelation'
   *     responses:
   *       200:
   *         description: related items
   *         examples:
   *           application/json:
   *             - id: 42
   *               name: sample
   *               color: blue
   *             - id: 44
   *               name: test
   *               color: yellow
   *
   * /db/{table}/item/{id}/related/{relation}/count:
   *   get:
   *     tags: ['db']
   *     summary: get related items count
   *     description: 'get number of related items.'
   *     produces:
   *       - application/json
   *     parameters:
   *       - $ref: '#/parameters/dbTable'
   *       - $ref: '#/parameters/dbItem'
   *       - $ref: '#/parameters/dbRelation'
   *     responses:
   *       200:
   *         description: item count
   *         examples:
   *           application/json: 42
   * @param {string} path
   * @param {string} key
   * @param {Router} router
   * @param {Request} req
   * @param {Response} res
   */
  async related(
    path,
    key,
    router,
    req,
    res) {
    req.url = path;
    set(req, [ 'query', 'filter', key ], get(req.params, 'id'));
    // @ts-ignore: internal Express method
    router.handle(req, res, noop);
  }

  /**
   * @param {string} path may differ from joinInfo.path
   * @param {mrest.JoinTableRelation} joinInfo
   * @param {Router} router
   * @param {Request} req
   * @param {Response} res
   */
  async relatedJoin(path, joinInfo, router, req, res) {
    const knex = await this.knexTxMgr.getKnex(req);
    var sel = knex(joinInfo.join.table)
    .select(joinInfo.join.relKey)
    .where(joinInfo.join.key, get(req.params, 'id'));
    set(req, [ 'query', 'filter', joinInfo.key || 'id', '$in' ], sel);
    req.url = path;
    // @ts-ignore: internal Express method
    router.handle(req, res, noop);
  }

  /**
   * @swagger
   * /db/{table}/item/{id}/related/{relation}:
   *   post:
   *     tags: ['db']
   *     summary: insert a new relation
   *     description: 'link two items using their relation table'
   *     parameters:
   *       - $ref: '#/parameters/dbTable'
   *       - $ref: '#/parameters/dbItem'
   *       - $ref: '#/parameters/dbRelation'
   *       - in: body
   *         name: related item(s) id(s)
   *         description: 'related item id or array of ids'
   *         required: true
   *         example: 1
   *     responses:
   *       200
   * @param {mrest.JoinTableRelation} joinInfo
   * @param {Request} req
   * @param {Response} res
   */
  async relatedJoinInsert(joinInfo, req, res) {
    this._noCache(res);
    const knex = await this.knexTxMgr.getKnex(req);
    const key = get(req.params, 'id');
    var query = knex(joinInfo.join.table);
    const data = isArray(req.body) ?
      map(req.body, (relKey) => (
        { [joinInfo.join.relKey]: relKey, [joinInfo.join.key]: key }
      )) :
      { [joinInfo.join.relKey]: req.body, [joinInfo.join.key]: key };

    return query.insert(data).then(noop);
  }

  /**
   * @swagger
   * /db/{table}/item/{id}/related/{relation}:
   *   delete:
   *     tags: ['db']
   *     summary: remove a relation between items
   *     description: unlin two items from their relation table
   *     parameters:
   *       - $ref: '#/parameters/dbTable'
   *       - $ref: '#/parameters/dbItem'
   *       - $ref: '#/parameters/dbRelation'
   *       - in: body
   *         name: related item(s) id(s)
   *         description: 'related item id or array of ids'
   *         required: true
   *         example: 1
   *     responses:
   *       200:
   *         description: relation(s) was/were removed
   *       404:
   *         description: item does not exist
  * @param {mrest.JoinTableRelation} joinInfo
  * @param {Request} req
  * @param {Response} res
  */
  async relatedJoinDelete(joinInfo, req, res) {
    this._noCache(res);
    const knex = await this.knexTxMgr.getKnex(req);
    const key = get(req.params, 'id');
    var query = knex(joinInfo.join.table);

    if (isArray(req.body)) {
      return query.where(joinInfo.join.key, key)
      .whereIn(joinInfo.join.relKey, req.body)
      .delete();
    }
    else {
      return query.where(joinInfo.join.key, key)
      .where(joinInfo.join.relKey, req.body)
      .delete();
    }
  }

  /**
   * @swagger
   * /db/{table}/item/{id}/related:
   *   get:
   *     tags: ['db']
   *     summary: list available relations
   *     description: list available relations
   *     produces:
   *       - application/json
   *     parameters:
   *       - $ref: '#/parameters/dbTable'
   *       - $ref: '#/parameters/dbItem'
   *     responses:
   *       200:
   *         description: list available relations
   *         schema:
   *           type: array
   *           items:
   *             type: string
   *         examples:
   *           application/json: [ 'test', 'sample' ]
   * @param {Request} req
   * @param {Response} res
   * @return {Promise<string[]>}
   */
  async listRelated(req, res) { /* jshint unused:false */
    return keys(this.options.related);
  }

  /**
   * @param {Request} req
   * @param {QueryBuilder} query
   * @return {QueryBuilder}
   */
  _buildQuery(req, query) {
    query = this._parseFilter(query, get(req, 'query.filter'));
    const queryStr = get(req, 'query.query');
    if (queryStr) {
      /* support extended query-string (see [qs](https://github.com/ljharb/qs)) */
      query = this._parseQuery(query, null,
        (typeof queryStr === 'object') ?
          queryStr : JSON.parse(queryStr.toString()),
        false);
    }
    const queryHdr = req.get('X-MRest-Query');
    if (queryHdr) {
      query = this._parseQuery(query, null, JSON.parse(queryHdr), false);
    }

    return query;
  }

  /**
   * @swagger
   * /db/{table}/count:
   *   get:
   *     tags: ['db']
   *     summary: get number of items in the table
   *     description: |+
   *       get number of items matching given filter rules or total number of items in the table.
   *     produces:
   *       - application/json
   *     parameters:
   *       - $ref: '#/parameters/dbTable'
   *       - $ref: '#/parameters/searchFilter'
   *       - $ref: '#/parameters/searchQuery'
   *       - $ref: '#/parameters/searchQueryHeader'
   *     responses:
   *       200:
   *         description: item count
   *         examples:
   *           application/json: 42
   * @param {Request} req
   * @param {Response} res
   * @return {Promise<number>}
   */
  async count(req, res) { /* jshint unused:false */
    this._noCache(res);
    const knex = await this.knexTxMgr.getKnex(req);
    return this._buildQuery(req, knex(this.table).first())
    .count('* as count')
    .then((ret) => get(ret, 'count'));
  }

  /**
   * @swagger
   * /db/{table}/item/{id}/streams/{name}:
   *   get:
   *     tags: ['db']
   *     summary: stream data
   *     description: stream data from a blob
   *     produces: application/octet-stream
   *     parameters:
   *       - $ref: '#/parameters/dbTable'
   *       - $ref: '#/parameters/dbItem'
   *     responses:
   *       200:
   *         description: the requested item
   *       404:
   *         description: item does not exist
   * @param  {string} column
   * @param  {Request} req
   * @param  {Response} res
   * @return {Promise<void>}
   */
  async streamGet(column, req, res) {
    this._noCache(res);
    const knex = await this.knexTxMgr.getKnex(req);
    const id = get(req.params, 'id');

    if (get(knex, [ 'client', 'dialect' ]) === 'oracle') {
      await oracle.streamGet(knex, this.table, column,
        this.options.key || '', id, res);
    }
    else {
      const ret = await knex(this.table).first()
      // @ts-ignore method not used if key not in options
      .where(this.options.key, id).column(column);

      if (isNil(ret)) {
        throw new HTTPError(404);
      }
      res.writeHead(200, { 'Content-Type': 'application/octet-stream' });
      res.write(ret[column]);
      res.end();
    }
  }

  /**
   * @swagger
   * /db/{table}/item/{id}/streams/{name}:
   *   put:
   *     tags: ['db']
   *     summary: stream-upload data
   *     description: stream data to a blob column
   *     content:
   *       application/octet-stream:
   *         schema:
   *           type: string
   *           format: binary
   *     parameters:
   *       - $ref: '#/parameters/dbTable'
   *       - $ref: '#/parameters/dbItem'
   *     responses:
   *       200:
   *         description: uploaded
   *       404:
   *         description: item does not exist
   * @param  {string} column
   * @param  {Request} req
   * @param  {Response} res
   * @return {Promise<void>}
   */
  async streamPut(column, req, res) {
    this._noCache(res);
    const knex = await this.knexTxMgr.getKnex(req);
    const id = get(req.params, 'id');

    if (get(knex, [ 'client', 'dialect' ]) === 'oracle') {
      await oracle.streamPut(knex, this.table, column,
        this.options.key || '', id, req);
    }
    else {
      var defer = makeDeferred();
      bodyParser.raw({ limit: '25mb' })(req, res, (/** @type {Error} */ err) => {
        if (err) {
          defer.reject(err);
        }
        else {
          var query = knex(this.table);

          return query // @ts-ignore method not used when no key
          .where(this.options.key, id)
          .update({ [column]: req.body })
          .then(() => defer.resolve(undefined), (err) => defer.reject(err));
        }
      });
      await defer.promise;
    }
  }

  /**
   * @swagger
   * /db:
   *   get:
   *     tags: ['db']
   *     summary: list available tables
   *     description: list available tables
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: list available tables
   *         schema:
   *           type: array
   *           items:
   *             type: string
   *         examples:
   *           application/json: [ 'test', 'sample' ]
   * @param {Request} req
   * @param {Response} res
   * @return {Promise<string[]>}
   */
  static async list(req, res) { /* jshint unused:false */
    return KnexREST.tables;
  }

  /**
   * @details simple filters are strings in the "key:value" form
   * @param {QueryBuilder} query
   * @param {string} filter
   * @return {QueryBuilder}
   */
  _parseSimpleFilter(query, filter) {
    /* csv formatted */
    return reduce(split(filter, ','), (query, filter) => {
      var i = filter.indexOf(':');
      if (i >= 0) {
        return query.where(this._toDB(filter.substr(0, i)), filter.substr(i + 1));
      }
      throw new HTTPError(400, 'Invalid filter: ' + filter);
    }, query);
  }

  /**
   * @param {QueryBuilder} query
   * @param {string|string[]|mrest.Query} filter
   * @return {QueryBuilder}
   */
  _parseFilter(query, filter) {
    if (isString(filter)) {
      // $FlowIgnore: checked above
      query = this._parseSimpleFilter(query, filter);
    }
    else if (isArray(filter)) {
      /* express has aggregated multiple arguments together */
      forEach/*:: <string, Array<string>, any> */(filter, (f) => {
        query = this._parseSimpleFilter(query, f);
      });
    }
    else {
      // $FlowIgnore: checked above
      return this._parseQuery(query, null, filter, false);
    }
    return query;
  }

  /**
   * @param {QueryBuilder} query
   * @param {?string} parentKey
   * @param {mrest.Query} filter
   * @param {boolean} inclusive
   * @return {QueryBuilder}
   */
  _parseQuery(
    query,
    parentKey,
    filter,
    inclusive) {
    /*
      filters can be `$op: value` (prop is parentKey), or `prop: value` ($op defaults to $eq)
    */
    // eslint-disable-next-line complexity
    forEach(filter, (value, key) => {
      const op = get(arrayOp, key);
      if (op && !isNil(value)) {
        /* if `key` is in `arrayOp` value is a sub-query (propName is parentKey)
          special case: { $not: null } must be handled with `isNil(value)` branch
        */
        query = op(this, parentKey, query, value);
      }
      else if (isPlainObject(value)) {
        /* if value is a plain object we're of the form `prop: { $op: value }` */
        query = this._parseQuery(query, key, value, inclusive);
      }
      else if (isNil(value)) {
        /* special handling for null values: `prop: { $op: null }` or `prop: null`
          SQL engine-dependent syntax must be used, ex: `prop IS NULL` (when `prop = NULL` fails)
        */
        const comp = get(nullOp, key, 'Null'); // handle `prop: null` case with default
        if (!isNil(comp)) { // nill comp are catch-all (ex: `$gte: null`)
          // @ts-ignore
          query = query[(inclusive ? 'orWhere' : 'where') + comp](this._toDB(parentKey || key));
        }
      }
      else {
        /*
          value is a direct-value
          if `key` is not in `compOp`, then `prop: value` style with
            `key=propName` (parentKey is null otherwise we have `prop: { prop: value }`)
            and `compOp=$eq`
        */
        const func = inclusive ? 'orWhere' : 'where';
        // @ts-ignore
        query = query[func](this._toDB(parentKey || key), get(compOp, key, '='), value);
      }
    });
    return query;
  }

  /**
   * @param {QueryBuilder} query
   * @param {?string} sort
   * @return {QueryBuilder}
   */
  _parseSort(query, sort) {
    if (isEmpty(sort)) {
      return query;
    }
    return reduce(split(sort, ','), (query, sort) => {
      var i = sort.lastIndexOf(':');
      if (i >= 0) {
        const
          column = sort.substr(0, i),
          direction = sort.substr(i + 1);

        if (includes([ 'desc', 'asc' ], toLower(direction))) {
          // $FlowIgnore: checked above
          return query.orderBy(this._toDB(column), direction);
        }
        /* wrong guess prob a column name */
      }
      else if (isString(sort)) {
        return query.orderBy(this._toDB(sort));
      }
      return query;
    }, query);
  }

  /**
   * @param {string} key
   * @return {string}
   */
  _toDB(key) {
    var map = get(this.options, [ 'revmapping', key ]);
    if (isNil(map)) {
      if (get(this.options, 'strict') && has(this.options, 'revmapping')) {
        throw new HTTPError(400, 'Invalid key: ' + key);
      }
      return key;
    }
    return map;
  }

  /**
   * @param {string} column
   * @return {string}
   */
  _fromDB(column) {
    return get(this.options, [ 'mapping', column ], column);
  }

  /**
   * @brief disable browser caching on reply
   * @param  {Response} res
   */
  _noCache(res) {
    res.set('Cache-control', 'no-store');
  }

  /**
   * @param {Router} app
   * @return {KnexREST}
   */
  register(app) {
    var path = '/' + this.path;
    app.get(path, wrap(this.search.bind(this)));
    app.get(path + '/explain', wrap(this.explain.bind(this)));

    app.get(path + '/count', wrap(this.count.bind(this)));
    if (this.options.key) {
      app.get(path + '/item/:id', wrap(this.get.bind(this)));
      if (this.options.rw) {
        app.delete(path + '/item/:id', wrap(this.delete.bind(this)));
        app.put(path + '/item/:id', KnexREST.JsonParser, wrap(this.update.bind(this)));
      }

      app.get(path + '/item/:id/related', wrap(this.listRelated.bind(this)));
      forEach(this.options.related, (desc, rel) => {
        if (has(desc, 'join')) {
          const joinDesc = /** @type {mrest.JoinTableRelation} */ (desc);
          app.get(path + '/item/:id/related/' + rel,
            this.relatedJoin.bind(this, '/' + desc.path, joinDesc, app));
          app.get(path + '/item/:id/related/' + rel + '/count',
            this.relatedJoin.bind(this, '/' + desc.path + '/count', joinDesc, app));
          if (this.options.rw) {
            app.post(path + '/item/:id/related/' + rel, KnexREST.JsonParser,
              wrap(this.relatedJoinInsert.bind(this, joinDesc)));
            app.delete(path + '/item/:id/related/' + rel, KnexREST.JsonParser,
              wrap(this.relatedJoinDelete.bind(this, joinDesc)));
          }
        }
        else if (has(desc, 'key')) {
          const directDesc = /** @type {mrest.DirectRelation} */ (desc);
          app.get(path + '/item/:id/related/' + rel,
            this.related.bind(this, '/' + desc.path, directDesc.key, app));
          app.get(path + '/item/:id/related/' + rel + '/count',
            this.related.bind(this, '/' + desc.path + '/count', directDesc.key, app));
        }
      });
      forEach(this.options.streams, (name, column) => {
        app.get(path + '/item/:id/streams/' + name,
          wrap(this.streamGet.bind(this, column)));
        if (this.options.rw) {
          app.put(path + '/item/:id/streams/' + name,
            wrap(this.streamPut.bind(this, column)));
        }
      });
    }
    if (this.options.rw) {
      app.post(path, KnexREST.JsonParser, wrap(this.insert.bind(this)));
      app.delete(path, KnexREST.JsonParser, wrap(this.deleteAll.bind(this)));
    }

    return this;
  }

  /**
   * @param {Router} app
   */
  static register(app) {
    app.get('/', wrap(KnexREST.list));
  }
}

KnexREST.JsonParser = bodyParser.json({ strict: false });
/** @type {Array<string>} table paths */
KnexREST.tables = [];

module.exports = KnexREST;
