// @ts-check
const
  path = require('path'),
  swaggerJSDoc = require('swagger-jsdoc'),
  swaggerUi = require('swagger-ui-express'),
  { get, cloneDeep, forEach, remove, set, unset, isNil, push, keys,
    has } = require('./lodash-ext');

/**
 * @typedef {import('express').Router} Router
 */

class KnexRESTSwagger {
  /**
   * @param {mrest.KnexRESTSwagger.Options} [options]
   * @param {mrest.KnexRESTSwagger.Info} [extraDoc]
   */
  constructor(options, extraDoc) {
    const definition = get(options, 'definition') || { info: { title: 'sample', version: '0.0.0' } };

    /** @type {any} */
    this.doc = swaggerJSDoc({ definition, apis: [ path.join(__dirname, '**.js') ] }) || {};
    this.extra = extraDoc;
  }

  /**
   * @param {mrest.KnexREST} service
   * @param {string} template
   * @param {string} path
   * @param {?string[]=} omit
   */
  swagTemplate(service, template, path, omit) {
    var doc = cloneDeep(get(this.doc, [ 'paths', template ]));
    forEach(doc, (elt) => {
      elt.tags = [ service.path ];
      /* remove resolved parameters */
      remove(elt.parameters, { $ref: '#/parameters/dbTable' });
      remove(elt.parameters, { $ref: '#/parameters/dbRelation' });
    });

    forEach(omit, (o) => unset(doc, o));
    forEach(get(this.extra, [ 'paths', path ]), (value, key) => {
      set(doc, key, value);
    });
    set(this.doc, [ 'paths', path ], doc);
    return doc;
  }

  /**
   * @param {Router} router
   */
  register(router) {
    router.get('/api-docs.json', (req, res) => {
      res.json(this.doc);
    });
    const opts = { swaggerOptions:
      { docExpansion: "none", displayRequestDuration: true } };

    router.use('/api-docs', swaggerUi.serveFiles(this.doc, opts),
      swaggerUi.setup(this.doc, opts));
  }

  /**
   * @brief generate swagger doc for given service
   * @param {mrest.KnexREST} service
   * @return {any}
   */
  swag(service) {
    var basePath = get(this.extra, 'basePath', '');
    var path = basePath + '/' + get(service, 'path');
    var doc;

    var tag = get(this.extra, [ 'tags', path ]);
    if (!isNil(tag)) {
      this.doc.tags = push(this.doc.tags,
        { name: service.path, description: tag });
    }

    /* table */
    doc = this.swagTemplate(service, '/db/{table}', path,
      (!service.options.key || !service.options.rw) ? [ 'post', 'delete' ] : null);

    this.swagTemplate(service, '/db/{table}/explain', path + '/explain');
    this.swagTemplate(service, '/db/{table}/count', path + '/count');

    /* items */
    if (service.options.key) {
      doc = this.swagTemplate(service, '/db/{table}/item/{id}', path + '/item/{id}',
        (!service.options.rw) ? [ 'delete', 'put' ] : null);

      if (service.options.related) {
        doc = this.swagTemplate(service, '/db/{table}/item/{id}/related',
          path + '/item/{id}/related');
        set(doc, "get.responses.200.examples.[application/json]",
          keys(service.options.related));
      }
      forEach(service.options.related, (desc, name) => {
        this.swagTemplate(service, '/db/{table}/item/{id}/related/{relation}',
          path + '/item/{id}/related/' + name,
          (service.options.rw && has(desc, 'join')) ? null : [ 'delete', 'post' ]);
        this.swagTemplate(service, '/db/{table}/item/{id}/related/{relation}/count',
          path + '/item/{id}/related/' + name + '/count');
      });
      forEach(service.options.streams, (name) => {
        this.swagTemplate(service, '/db/{table}/item/{id}/streams/{name}',
          path + '/item/{id}/streams/' + name,
          (!service.options.rw) ? [ 'put' ] : null);
      });
    }
    return this.doc;
  }
}

module.exports = KnexRESTSwagger;
