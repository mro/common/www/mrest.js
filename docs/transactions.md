# Transactions

This library supports transactions that can be committed or rollbacked by the user.

## Enabling/Disabling Transaction Manager

Transactional functionalities are enabled by default. It can be disabled by passing

```json
{
    transactions: false
}
```

to `KnexServer` options.

## Transactions via REST API (client side)

This library expose some REST API in order to list/create/commit/rollback a transaction. Here the swagger:

![Transaction REST API Swagger](./transactions_swagger.png)

### List: GET `/db/transaction`

This endpoint lists all the currently active transactions. Example:

```bash
curl "http://localhost:8080/db/transaction"
```

Response:

```json
[
  {
    "transactionId": "528a1d2a-541a-41b2-ba51-714b03de3de8",
    "expiry": 9005
  }
]
```

### Create: POST `/db/transaction`

This endpoint will create a new transaction. It will return a transaction Id to be used in all the queries as a uri parameter (`?transactionId=<id>`). We can also specify the expiry time on the create request. Example:

```bash
curl -X POST 'http://localhost:8080/db/transaction' -H "Content-Type: application/json"  -d '{"expiryTimeMs": 10000}'
```

Response:

```json
{
  "transactionId": "528a1d2a-541a-41b2-ba51-714b03de3de8"
}
```

### Commit: POST `/db/transaction/{id}`

This endpoint will commit an existing transaction. Example:

```bash
curl -X POST 'http://localhost:8080/db/transaction/528a1d2a-541a-41b2-ba51-714b03de3de8'
```

Response: Status `200` if transaction is committed or `400` if transaction is not found

### Rollback: DELETE `/db/transaction/{id}`

This endpoint will commit an existing transaction. Example:

```bash
curl -X DELETE 'http://localhost:8080/db/transaction/528a1d2a-541a-41b2-ba51-714b03de3de8'
```

Response: Status `200` if transaction is rollbacked or `400` if transaction is not found

### Complete example with REST API

```bash
# Create a transaction
curl -X POST 'http://localhost:8080/db/transaction' -H "Content-Type: application/json"  -d '{"expiryTimeMs": 10000}'
>> { "transactionId": "528a1d2a-541a-41b2-ba51-714b03de3de8" }

# Update an item
curl -X PUT "http://localhost:8080/db/runs/item/534?transactionId=528a1d2a-541a-41b2-ba51-714b03de3de8" \
  -H "Content-Type: application/json" \
  -d '{"title":"New Title"}'

# Delete an item
curl -X DELETE "http://localhost:8080/db/runs/item/534?transactionId=528a1d2a-541a-41b2-ba51-714b03de3de8"

# Commit transaction
curl -X POST 'http://localhost:8080/db/transaction/528a1d2a-541a-41b2-ba51-714b03de3de8'
```

## Transactions via HTTP req binding (server side)

Another way to create and use a transaction that is suitable on backend side, is to create a new transaction and bind it to req object. For example, let's assume you are in your MyKnexServer class that extends KnexServer object (so you have access to this.knex member):

```js
try {
    // Create a new transaction
    await this.knex.transaction(async (/** @type {Knex} */ knexTransaction) => {
    // knexTransaction is the object you need to bind to the req fake request.

    // Insert a row within a transaction
    const batchId = await this.tables['myTable'].insert( // @ts-ignore fake Request
    { get: noop, body: { field: "value" } , knexTransaction }, { set: noop });
    console.info('batchId', batchId);


    // Get inserted row within a transaction
    let res = await this.tables['myTable'].search(// @ts-ignore fake Request
    { get: noop, query: { filter: "field:value" }, knexTransaction }, { set: noop });

    // If no error occours, the transaction will be committed at the end of this section-
} catch (error) {
    // If an error occours, the transaction will be automatically rollbacked
    throw new HTTPError(500);
}
```

NB: Rest API mode has higher priority then req binding.
