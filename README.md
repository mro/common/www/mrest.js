# MongoDB like REST API

This library implements part of the MongoDB syntax as a REST API back-end.

For details about the [MongoDB syntax](https://docs.mongodb.com/manual/reference/operator/query/).

# Examples

To run an SQlite sample database:
```bash
npm run stub
```

Some sample curl queries:
```bash
# Get all (max: 100) runs items
curl "http://localhost:8080/db/runs"

# Get a specific run
curl "http://localhost:8080/db/runs/item/534"

# Query for some runs (same query different forms)
# simple filter query
curl "http://localhost:8080/db/runs?filter=title:No%20Title"
# url-encoded query
curl 'http://localhost:8080/db/runs?query=%7B%22title%22%3A%22No+Title%22%7D'
# In-headers query
curl 'http://localhost:8080/db/runs' -H 'X-MRest-Query: {"title":"No Title"}'

# Update an item
curl -X PUT "http://localhost:8080/db/runs/item/534" \
  -H "Content-Type: application/json" \
  -d '{"title":"New Title"}'

# Delete an item
curl -X DELETE "http://localhost:8080/db/runs/item/534"

# Create an item
curl -X POST 'http://localhost:8080/db/runs' -H "Content-Type: application/json"  -d '{"id":534,"start":"2000-12-31 22:59:00.000","stop":"2000-12-31 23:00:00.000","title":"New Title","description":"534 2000-12-31T21:59:00.000Z","titleOriginal":"No Title","descOriginal":"534 2000-12-31T21:59:00.000Z","runNumber":489,"castorFolder":"No information","totalProtons":0,"runStatus":"unknown","dataStatus":"unknown","detSetupId":1,"matSetupId":1}'
```

To better test the API a swagger live documentation is available http://localhost:8080/db/api-docs.

# Transactions

This module support db transactions. More information in the [documentation](./docs/transactions.md).

# Configuration

The database configuration is split in three different parts.

## Database connection

As described in [http://knexjs.org/#Installation-client](Knex.js) documentation:
```js
// SQlite configuration
{
  client: 'sqlite3',
  useNullAsDefault: true,
  connection: { filename: path.join(env.dir.name, 'db.sql') },
  definition: {
    info: {
      title: 'My DB',
      version: '1.0.0' }
  }
}

// Oracle configuration
{
  client: "oracledb",
  connection: {
    user: "ntof",
    password: "xxx",
    connectString: "pdb-s.cern.ch:10121/PDB_CERNDB1.cern.ch" },
  definition: {
    info: {
      title: 'My DB',
      version: '1.0.0' }
  }
}
```

## Database description
```js
{
  // table path (will be used in url and to refer to this table)
  runs: {
    // table name in db (optional, if different from path)
    table: "RUNS",
    // primary key for the table
    // optional, /item paths are not available if not set
    key: "RUN_ID",
    // columns renaming
    mapping: {
      RUN_ID: "id",
      RUN_START: "start",
      RUN_STOP: "stop",
      RUN_TITLE: "title",
      RUN_DESCRIPTION: "description",
      RUN_RUN_NUMBER: "runNumber",
    },
    // large blob columns
    streams: {
      RUN_DOCUMENT: "document"
    },
    // strict mode, any unmapped column will be rejected in strict mode
    // optional, default: false
    strict: true,
    // read-write mode, add put/post/delete semantic
    // optional, default: false
    rw: true,
    // table relation ships
    related: {
      // another service that must be described in this document
      documents: {
        // path to the other service
        path: "documents",
        // N-N relationship using an intermediate table
        join: {
          // intermediate table name
          table: "REL_DOCS_RUNS",
          // key column
          key: "RUN_ID",
          // relation item key column
          relKey: "DOC_ID"
        }
      },
      // another service that must be described in this document
      comments: {
        // path to the other service
        path: "comments",
        // mapped column name in the other service leading to this table
        key: "runId"
      },
    }
  },
  // documents, comments...
}
```

## Additional documentation
```js
{
  // routed db path
  basePath: '/db',
  tags: { // documentation tags (group) descriptions
    "/db/documents": "experiment's runs' documents",
    "/db/runs": "experiment's runs database"
  },
  paths: { // swagger documentation examples/doc injector
    "/db/documents": {
      // json path of the element to inject
      "get.responses.200.examples.[application/json]": [
        {
          "id": 86,
          "title": "Test2.pdf",
          "name": "Test2.pdf",
          "size": 81255,
          "document": null
        }
      ]
    }
  }
  //...
}
```

The generated swagger json file can be retrieved from http://localhost:8080/db/api-docs.json

# Tests

To run the unit-tests:
```bash
npm run test
```

# Usage

To use this library as a dependency:
```bash
# Install it
npm install "git+https://gitlab.cern.ch/apc/common/www/mrest.js.git"
```
