// @flow
const
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { expect } = require('chai'),
  { delay } = require('@cern/prom'),
  { get, clone, assign } = require('lodash'),
  sa = require('superagent'),
  Helpers = require('./Helpers');

var KnexServer = require('../src/KnexServer'); /* will start the server */

describe('KnexREST transactions', function() {

  var env = Helpers.defaultEnv();
  var config = {
    testTable: { table: 'testTable', key: 'id', rw: true },
    testTable2: { table: 'testTable2', key: 'id', rw: true }
  };
  /** @type KnexServer */
  var server;

  beforeEach(async function() {
    await env.db.schema.createTable('testTable', function(table) {
      table.increments();
      table.text('name').notNullable();
    });
    await env.db('testTable').insert([
      { name: 'toto' },
      { name: 'titi' },
      { name: 'titi' }
    ]);
    await env.db.schema.createTable('testTable2', function(table) {
      table.increments();
      table.text('name').notNullable();
    });
    await env.db('testTable2').insert([
      { name: 'tata' }
    ]);

    server = new KnexServer(env.dbConfig, config, {});
  });

  afterEach(async function() {
    server.release();
  });

  it('fails to create a transaction when KnexTransactionManager is disabled', async function() {
    server.release();
    server = new KnexServer(assign(clone(env.dbConfig), { transactions: false }), config, {});
    server.register(env.app);

    var ret;
    await sa.post('localhost:1234/db/transaction').send()
    .send({ name: 'test1' }).catch((retErr) => { ret = retErr; });
    expect(ret).to.deep.include({ status: 404 });

    // Check that insert still works
    ret = await sa.post(`localhost:1234/testTable`)
    .send({ name: 'test1' });
    expect(ret).to.deep.include({ status: 200 });
    expect(ret.body).to.equal(4);

    ret = await sa.get('localhost:1234/testTable')
    .query({ filter: 'name:test1' });
    expect(ret).to.deep.include({ status: 200 });
    expect(ret.body).to.have.length(1);
    expect(ret.body[0]).to.deep.include({ id: 4, name: 'test1' });
  });

  it('fails when transactionId does not exist', async function() {
    server.register(env.app);
    var ret;
    await sa.post(`localhost:1234/testTable?transactionId=wrongId`)
    .send({ name: 'test1' }).catch((retErr) => { ret = retErr; });
    expect(ret).to.deep.include({ status: 400 });
  });

  it('fails when transactionId is expired', async function() {
    server.register(env.app);
    // Create knex transaction
    const uuid = await sa.post('localhost:1234/db/transaction').send({ expiryTimeMs: 10 })
    .then((ret) => get(ret, 'body.transactionId'));
    expect(uuid).is.not.undefined();
    await delay(20);

    var ret;
    await sa.post(`localhost:1234/testTable?transactionId=${uuid}`)
    .send({ name: 'test1' }).catch((retErr) => { ret = retErr; });
    expect(ret).to.deep.include({ status: 400 });
  });

  it('can insert items and commit', async function() {
    server.register(env.app);
    // Create knex transaction
    const uuid = await sa.post('localhost:1234/db/transaction').send()
    .then((ret) => get(ret, 'body.transactionId'));
    expect(uuid).is.not.undefined();

    // Check transactin is displayed in the List API
    const listTrx = await sa.get('localhost:1234/db/transaction');
    expect(listTrx).to.deep.include({ status: 200 });
    expect(listTrx.body).to.have.length(1);
    expect(listTrx.body[0]).to.have.property('transactionId', uuid);

    // Do queries
    var ret = await sa.post(`localhost:1234/testTable?transactionId=${uuid}`)
    .send({ name: 'test1' });
    expect(ret).to.deep.include({ status: 200 });
    expect(ret.body).to.equal(4);

    ret = await sa.post(`localhost:1234/testTable2?transactionId=${uuid}`)
    .send({ name: 'test2' });
    expect(ret).to.deep.include({ status: 200 });
    expect(ret.body).to.equal(2);

    // Commit transaction
    var commitRet =  await sa.post(`localhost:1234/db/transaction/${uuid}`).send();
    expect(commitRet).to.deep.include({ status: 200 });
    expect(commitRet.body).to.be.true();

    // Check db
    ret = await sa.get('localhost:1234/testTable')
    .query({ filter: 'name:test1' });
    expect(ret).to.deep.include({ status: 200 });
    expect(ret.body).to.have.length(1);
    expect(ret.body[0]).to.deep.include({ id: 4, name: 'test1' });

    ret = await sa.get('localhost:1234/testTable2')
    .query({ filter: 'name:test2' });
    expect(ret).to.deep.include({ status: 200 });
    expect(ret.body).to.have.length(1);
    expect(ret.body[0]).to.deep.include({ id: 2, name: 'test2' });
  });

  it('can insert items and rollback', async function() {
    server.register(env.app);
    // Create knex transaction
    const uuid = await sa.post('localhost:1234/db/transaction').send()
    .then((ret) => get(ret, 'body.transactionId'));
    expect(uuid).is.not.undefined();

    // Do query1
    var ret = await sa.post(`localhost:1234/testTable?transactionId=${uuid}`)
    .send({ name: 'test1' });
    expect(ret).to.deep.include({ status: 200 });
    expect(ret.body).to.equal(4);

    // Check db1
    ret = await sa.get(`localhost:1234/testTable?transactionId=${uuid}`)
    .query({ filter: 'name:test1' });
    expect(ret).to.deep.include({ status: 200 });
    expect(ret.body).to.have.length(1);
    expect(ret.body[0]).to.deep.include({ id: 4, name: 'test1' });

    // Do query2
    ret = await sa.post(`localhost:1234/testTable2?transactionId=${uuid}`)
    .send({ name: 'test2' });
    expect(ret).to.deep.include({ status: 200 });
    expect(ret.body).to.equal(2);

    ret = await sa.get(`localhost:1234/testTable2?transactionId=${uuid}`)
    .query({ filter: 'name:test2' });
    expect(ret).to.deep.include({ status: 200 });
    expect(ret.body).to.have.length(1);
    expect(ret.body[0]).to.deep.include({ id: 2, name: 'test2' });

    // Rollback
    var commitRet =  await sa.delete(`localhost:1234/db/transaction/${uuid}`).send();
    expect(commitRet).to.deep.include({ status: 200 });
    expect(commitRet.body).to.be.true();

    // Check inserted are not there anymore
    ret = await sa.get(`localhost:1234/testTable`)
    .query({ filter: 'name:test1' });
    expect(ret).to.deep.include({ status: 200 });
    expect(ret.body).to.have.length(0);

    ret = await sa.get(`localhost:1234/testTable2`)
    .query({ filter: 'name:test2' });
    expect(ret).to.deep.include({ status: 200 });
    expect(ret.body).to.have.length(0);
  });

});
