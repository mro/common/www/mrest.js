// @flow
const
  { describe, it, beforeEach } = require('mocha'),
  { expect } = require('chai'),
  _ = require('lodash'),
  sa = require('superagent'),
  axios = require('axios').default,
  Helpers = require('./Helpers');

var KnexREST = require('../src/KnexREST'); /* will start the server */

describe('KnexREST tests', function() {

  var env = Helpers.defaultEnv();

  beforeEach(async function() {
    await env.db.schema.createTable('testTable', function(table) {
      table.increments();
      table.text('name');
      table.integer('value');
    });
    await env.db('testTable').insert([
      { name: 'toto', value: 42 },
      { name: 'titi', value: 43 },
      { name: 'titi', value: 44 }
    ]);
  });

  it('can list databases', async function() {
    var service = new KnexREST(env.mgr, 'testTable', {});
    service.register(env.app);
    KnexREST.register(env.app); /* base services */

    const ret = await sa.get('localhost:1234');
    expect(ret).to.deep.include({ status: 200, body: [ 'testTable' ] });
  });

  it('can list all items', async function() {
    (new KnexREST(env.mgr, 'testTable', {})).register(env.app);

    const ret = await sa.get('localhost:1234/testTable');
    expect(ret).to.deep.include({ status: 200, body: [
      { id: 1, name: 'toto', value: 42 },
      { id: 2, name: 'titi', value: 43 },
      { id: 3, name: 'titi', value: 44 }
    ] });
  });

  it('fails to list all items on unknown table', async function() {
    await sa.get('localhost:1234/doesNotExist')
    .then(
      () => { throw 'should fail'; },
      (ret) => expect(ret).include({ status: 404 })
    );
  });

  describe('items filtering', function() {
    _.forEach([
      {
        query: { filter: { name: 'titi' } },
        reply: [
          { id: 2, name: 'titi', value: 43 },
          { id: 3, name: 'titi', value: 44 }
        ]
      },
      {
        query: { filter: { value: { $lte: 43 } }, sort: 'name' },
        reply: [
          { id: 2, name: 'titi', value: 43 },
          { id: 1, name: 'toto', value: 42 }
        ]
      },
      {
        query: { filter: { value: { $gte: 43 } }, sort: 'value:DESC' },
        reply: [
          { id: 3, name: 'titi', value: 44 },
          { id: 2, name: 'titi', value: 43 }
        ]
      },
      {
        query: { filter: { value: { $gte: 43 } }, sort: 'value:ASC' },
        reply: [
          { id: 2, name: 'titi', value: 43 },
          { id: 3, name: 'titi', value: 44 }
        ]
      },
      {
        query: { filter: { name: { $like: "Ti%" } }, sort: 'value:ASC' },
        reply: [
          { id: 2, name: 'titi', value: 43 },
          { id: 3, name: 'titi', value: 44 }
        ]
      },
      {
        query: { filter: { name: { $not: { $like: "ti%" } } }, sort: 'value:ASC' },
        reply: [
          { id: 1, name: 'toto', value: 42 }
        ]
      },
      {
        query: { filter: { value: { $in: [ 43, 44 ] } }, sort: 'value:ASC' },
        reply: [
          { id: 2, name: 'titi', value: 43 },
          { id: 3, name: 'titi', value: 44 }
        ]
      },
      {
        query: { filter: { value: { $nin: [ 43, 44 ] } }, sort: 'value:ASC' },
        reply: [
          { id: 1, name: 'toto', value: 42 }
        ]
      },
      {
        query: { filter: [ 'name:titi', 'value:43' ] },
        reply: [
          { id: 2, name: 'titi', value: 43 }
        ]
      },
      {
        query: { filter: 'name:titi,value:43' },
        reply: [
          { id: 2, name: 'titi', value: 43 }
        ]
      },
      {
        query: { sort: 'name,value:desc' },
        reply: [
          { id: 3, name: 'titi', value: 44 },
          { id: 2, name: 'titi', value: 43 },
          { id: 1, name: 'toto', value: 42 }
        ]
      },
      {
        query: { sort: [ 'name', 'value:desc' ] },
        reply: [
          { id: 3, name: 'titi', value: 44 },
          { id: 2, name: 'titi', value: 43 },
          { id: 1, name: 'toto', value: 42 }
        ]
      },
      {
        /* using $or array operator */
        query: { query: JSON.stringify({ $or: [ { name: 'toto' }, { value: 44 } ] }), sort: [ 'name' ] },
        reply: [
          { id: 3, name: 'titi', value: 44 },
          { id: 1, name: 'toto', value: 42 }
        ]
      },
      {
        /* using $or and nested $and */
        query: { filter: { $or:
          // $FlowIgnore: using numbers as keys works
          { 0: { $and: { 0: { name: 'toto' }, 1: { value: { $gte: 42 } } } },
          // $FlowIgnore: using numbers as keys works
            1: { value: 44 } }
        }, sort: [ 'name' ] },
        reply: [
          { id: 3, name: 'titi', value: 44 },
          { id: 1, name: 'toto', value: 42 }
        ]
      },
      {
        /* using $or and nested $and json syntax*/
        query: { query: JSON.stringify({ $or:
          [ { $and: [ { name: 'toto' }, { value: { $gte: 42 } } ] }, { value: 44 } ]
        }), sort: [ 'name' ] },
        reply: [
          { id: 3, name: 'titi', value: 44 },
          { id: 1, name: 'toto', value: 42 }
        ]
      },
      {
        /* using $not and $or */
        query: { query: JSON.stringify({ $not: { $or: [ { value: 44 }, { value: 42 } ] } }) },
        reply: [
          { id: 2, name: 'titi', value: 43 }
        ]
      },
      {
        /* using $not and $in */
        query: { query: JSON.stringify({ value: { $not: { $in: [ 44, 42 ] } } }) },
        reply: [
          { id: 2, name: 'titi', value: 43 }
        ]
      },
      {
        /* $not operator */
        query: { query: JSON.stringify({ $not: { $or: [ { value: 44 }, { value: 42 } ] } }) },
        reply: [
          { id: 2, name: 'titi', value: 43 }
        ]
      },
      { /* using $or and regular test */
        query: { query: JSON.stringify({ name: 'titi', '$or': [ { value: 43 }, { value: 44 } ] }) },
        reply: [
          { id: 2, name: 'titi', value: 43 },
          { id: 3, name: 'titi', value: 44 }
        ]
      }
    ], function(params) {
      it(`can filter items with: ${JSON.stringify(params.query)}`, async function() {
        (new KnexREST(env.mgr, 'testTable', { key: 'id' })).register(env.app);

        await sa.get('localhost:1234/testTable')
        .query(params.query)
        .then((ret) => expect(ret.body).eql(params.reply));
      });

      it(`can count items with: ${JSON.stringify(params.query)}`, async function() {
        (new KnexREST(env.mgr, 'testTable', { key: 'id' })).register(env.app);

        await sa.get('localhost:1234/testTable/count')
        .query(params.query)
        .then((ret) => expect(ret.body).eql(_.size(params.reply)));
      });
    });
  });

  it('can use headers to query', async function() {
    var service = new KnexREST(env.mgr, 'testTable', { key: 'id' });
    service.register(env.app);
    KnexREST.register(env.app); /* base services */

    var ret = await sa.get('localhost:1234/testTable')
    .set('X-MRest-Query', JSON.stringify({ $not: { $or: [ { value: 44 }, { value: 42 } ] } }));
    expect(ret).to.deep.include({ status: 200 });
    expect(ret.body).to.deep.equal([ { id: 2, name: 'titi', value: 43 } ]);
  });

  it('can use extended URL-encoded objects in query', async function() {
    /*
      express may use [qs](https://github.com/ljharb/qs) to prepare query-string,
      even tho this is not recommended since all values will be converted to "string"
      mrest should support it
    */
    var service = new KnexREST(env.mgr, 'testTable', { key: 'id' });
    service.register(env.app);
    KnexREST.register(env.app); /* base services */

    var ret = await axios.get('http://localhost:1234/testTable',
      { params: { query: { $and: [ { name: "titi" }, { value: 43 }  ] } } });
    expect(ret).to.deep.include({ status: 200 });
    expect(ret.data).to.deep.equal([ { id: 2, name: 'titi', value: 43 } ]);

    /* both requests are equivalent, objects should be rebuilt by express */
    ret = await axios.get('http://localhost:1234/testTable?query[$and][0][name]=titi&query[$and][1][value]=43');
    expect(ret).to.deep.include({ status: 200 });
    expect(ret.data).to.deep.equal([ { id: 2, name: 'titi', value: 43 } ]);
  });

  it('can explain a query', async function() {
    (new KnexREST(env.mgr, 'testTable', { key: 'id' })).register(env.app);

    const ret = await sa.get('localhost:1234/testTable/explain')
    .query({ query: JSON.stringify(
      { $not: { $or: [ { value: 44 }, { value: 42 } ] } }) });

    expect(ret.status).eql(200);
    expect(ret.body).to.deep.equal("select * from `testTable` where not ((`value` = 44 or `value` = 42)) limit 100");
  });

  it('can get an item', async function() {
    (new KnexREST(env.mgr, 'testTable', { key: 'id' })).register(env.app);

    const ret = await sa.get('localhost:1234/testTable/item/1');
    expect(ret.status).eql(200);
    expect(ret.body).eql({ id: 1, name: 'toto', value: 42 });
  });

  it('fails to retrieve unknown item', async function() {
    (new KnexREST(env.mgr, 'testTable', { key: 'id' })).register(env.app);

    await sa.get('localhost:1234/testTable/item/1234')
    .then(
      () => { throw 'should fail'; },
      (ret) => expect(ret.status).eql(404)
    );
  });

  it('can count items', async function() {
    (new KnexREST(env.mgr, 'testTable', {})).register(env.app);

    const ret = await sa.get('localhost:1234/testTable/count');
    expect(ret.body).eql(3);
    expect(ret.status).eql(200);
  });

  it('can map columns', async function() {
    (new KnexREST(env.mgr, 'testTable', {
      key: 'id',
      mapping: { 'name': 'publicName', 'value': 'publicValue' }
    })).register(env.app);

    const ret = await sa.get('localhost:1234/testTable')
    .query({ filter: { publicName: 'toto' }, sort: 'publicName' });

    expect(ret.body).eql([ { id: 1, publicName: 'toto', publicValue: 42 } ]);
    expect(ret.status).eql(200);
  });

  it('strictly maps columns', async function() {
    var knexRest = new KnexREST(env.mgr, 'testTable', {
      key: 'id',
      mapping: { 'name': 'publicName', 'value': 'publicValue' }
    });
    knexRest.register(env.app);

    /* valid request but not using mapping */
    const ret = await sa.get('localhost:1234/testTable')
    .query({ filter: { name: 'toto' }, sort: 'publicName' });
    expect(ret.body).eql([ { id: 1, publicName: 'toto', publicValue: 42 } ]);
    expect(ret.status).eql(200);

    knexRest.options.strict = true; /* turn on strict mode */
    await sa.get('localhost:1234/testTable')
    .query({ filter: { name: 'toto' }, sort: 'publicName' })
    .then(
      () => { throw 'should fail'; },
      (err) => {
        expect(err).have.property('status', 400);
        expect(err.response).have.property('text', 'Invalid key: name');
      }
    );
  });

  it('strictly filters non-mapped columns', async function() {
    /* when strict mode is active, filter out unmapped columns */
    var knexRest = new KnexREST(env.mgr, 'testTable', {
      key: 'id',
      mapping: { 'name': 'publicName' },
      strict: true
    });
    knexRest.register(env.app);

    const ret = await sa.get('localhost:1234/testTable')
    .query({ filter: { publicName: 'toto' } });
    expect(ret.body).eql([ { id: 1, publicName: 'toto' } ]);
    expect(ret.status).eql(200);
  });

  it('can rename a table', async function() {
    (new KnexREST(env.mgr, 'testTable', { key: 'id', path: 'test' }))
    .register(env.app);

    const ret = await sa.get('localhost:1234/test/count');
    expect(ret.body).eql(3);
    expect(ret.status).eql(200);
  });

  describe('invalid requests', function() {
    _.forEach([
      { query: { sort: 'wrong' }, status: 400, text: 'Invalid key: wrong' },
      { query: { filter: 'bad' }, status: 400, text: 'Invalid filter: bad' },
      { query: { filter: 'bad:42' }, status: 400, text: 'Invalid key: bad' }
    ], function(params) {
      it(`rejects invalid request: ${JSON.stringify(params.query)}`, async function() {
        (new KnexREST(env.mgr, 'testTable', {
          key: 'id', strict: true,
          mapping: { 'name': 'publicName', 'value': 'publicValue' }
        })).register(env.app);

        await sa.get('localhost:1234/testTable')
        .query(params.query)
        .then(
          () => { throw 'should fail'; },
          (err) => {
            expect(err).have.property('status', params.status);
            expect(err.response).have.property('text', params.text);
          }
        );
      });
    });
  });
});
