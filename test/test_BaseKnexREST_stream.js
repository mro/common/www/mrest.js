// @ts-check
const
  { describe, it, beforeEach } = require('mocha'),
  { expect } = require('chai'),
  { makeDeferred } = require('@cern/prom'),
  fs = require('fs'),
  sa = require('superagent'),
  Helpers = require('./Helpers');

var KnexREST = require('../src/KnexREST'); /* will start the server */

describe('KnexREST rw tests', function() {

  var env = Helpers.defaultEnv();

  beforeEach(async function() {
    await env.db.schema.createTable('testTable', function(table) {
      table.increments();
      table.text('name').notNullable();
      table.binary('value').notNullable();
    });
    await env.db('testTable').insert([
      { name: 'toto', value: Buffer.from("test", "utf-8") },
      { name: 'titi', value: Buffer.from("test2", "utf-8") }
    ]);
  });

  it('can stream-download blob content', async function() {
    var service = new KnexREST(env.mgr, 'testTable', { key: 'id', rw: true,
      streams: { value: 'valueStream' }
    });
    service.register(env.app);
    KnexREST.register(env.app); /* base services */

    var ret;
    ret = await sa.get('localhost:1234/testTable/item/1/streams/valueStream')
    .buffer(true);
    expect(ret).to.deep.include({ status: 200 });
    expect(ret.body).to.deep.equal(Buffer.from("test", "utf-8"));
  });

  it('can stream-upload blob content', async function() {
    var service = new KnexREST(env.mgr, 'testTable', { key: 'id', rw: true,
      streams: { value: 'valueStream' }
    });
    service.register(env.app);
    KnexREST.register(env.app); /* base services */

    var ret;

    const defer = makeDeferred();
    const req = sa.put('localhost:1234/testTable/item/1/streams/valueStream')
    .type('application/octet-stream')
    .on('response', (res) => defer.resolve(res))
    .on('error', (err) => defer.reject(err));
    // @ts-ignore
    fs.createReadStream('test/data/testFile.txt').pipe(req);
    expect(await defer.promise).to.deep.include({ status: 200 });

    ret = await sa.get('localhost:1234/testTable/item/1/streams/valueStream')
    .buffer(true);
    expect(ret).to.deep.include({ status: 200 });
    expect(ret.body).to.deep.equal(fs.readFileSync('test/data/testFile.txt'));
  });

  it('fails on stream error', async function() {
    var service = new KnexREST(env.mgr, 'invalidTable', { key: 'id', rw: true,
      streams: { value: 'valueStream' }
    });
    service.register(env.app);
    KnexREST.register(env.app); /* base services */

    const defer = makeDeferred();
    const req = sa.put('localhost:1234/invalidTable/item/1/streams/valueStream')
    .type('application/octet-stream')
    .on('response', (res) => defer.resolve(res))
    .on('error', (err) => defer.reject(err));
    // @ts-ignore
    fs.createReadStream('test/data/testFile.txt').pipe(req);
    await defer.promise.then(
      (res) => expect(res).to.deep.include({ ok: false, status: 500 }));

    await sa.get('localhost:1234/invalidTable/item/1/streams/valueStream')
    .buffer(true)
    .then(
      () => { throw new Error('should fail'); },
      (err) => expect(err).to.deep.include({ status: 500 }));
  });
});
