// @flow
const
  express = require('express'),
  { describe, it, beforeEach } = require('mocha'),
  { expect } = require('chai'),
  sa = require('superagent'),
  Helpers = require('./Helpers');

var KnexREST = require('../src/KnexREST'); /* will start the server */

describe('KnexREST db relations', function() {
  /** @type { ReturnType<typeof Helpers.defaultEnv> & {
   *    infoDesc: mrest.DirectRelation
   *  }}
   */ // @ts-ignore
  var env = Helpers.defaultEnv();

  beforeEach(function() {
    env.infoDesc = {
      path: 'infoTable',
      key: 'testID'
    };
  });

  beforeEach(async function() {
    await env.db.schema.createTable('testTable', function(table) {
      table.increments();
      table.text('name');
      table.integer('value');
    });
    await env.db('testTable').insert([
      { id: 1, name: 'toto', value: 42 },
      { id: 2, name: 'titi', value: 43 },
      { id: 3, name: 'titi', value: 44 }
    ]);
  });

  beforeEach(async function() {
    await env.db.schema.createTable('infoTable', function(table) {
      table.integer('testID');
      table.text('info');
    });
    await env.db('infoTable').insert([
      { testID: 2, info: 'info1' },
      { testID: 2, info: 'info2' }
    ]);
  });

  it('can list relations', async function() {
    (new KnexREST(env.mgr, 'infoTable', {})).register(env.app);
    (new KnexREST(env.mgr, 'testTable',
      { key: 'id', related: { info: env.infoDesc } })).register(env.app);

    const ret = await sa.get('localhost:1234/testTable/item/2/related');
    expect(ret).to.deep.include({ status: 200, body: [ 'info' ] });
  });

  it('can list items using a relation', async function() {
    (new KnexREST(env.mgr, 'infoTable', {})).register(env.app);
    (new KnexREST(env.mgr, 'testTable',
      { key: 'id', related: { info: env.infoDesc } })).register(env.app);

    const ret = await sa.get('localhost:1234/testTable/item/2/related/info');
    expect(ret).to.deep.include({ status: 200, body: [
      { testID: 2, info: 'info1' },
      { testID: 2, info: 'info2' }
    ] });
  });

  it('can filter items in a relation', async function() {
    var rt = express.Router();
    (new KnexREST(env.mgr, 'infoTable', {})).register(rt);
    (new KnexREST(env.mgr, 'testTable',
      { key: 'id', related: { info: env.infoDesc } })).register(rt);
    env.app.use('/pwet', rt);

    const ret = await sa.get('localhost:1234/pwet/testTable/item/2/related/info')
    .query({ filter: { info: 'info1' } });
    expect(ret).to.deep.include({ status: 200, body: [
      { testID: 2, info: 'info1' }
    ] });
  });

  it('can count items in a relation', async function() {
    (new KnexREST(env.mgr, 'infoTable', {})).register(env.app);
    (new KnexREST(env.mgr, 'testTable',
      { key: 'id', related: { info: env.infoDesc } })).register(env.app);

    const ret = await sa.get('localhost:1234/testTable/item/2/related/info/count');
    expect(ret).to.deep.include({ status: 200, body: 2 });
  });
});
