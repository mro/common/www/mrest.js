// @flow
const
  { describe, it, beforeEach } = require('mocha'),
  { expect } = require('chai'),
  _ = require('lodash'),
  sa = require('superagent'),
  Helpers = require('./Helpers');

var KnexREST = require('../src/KnexREST'); /* will start the server */

describe('KnexREST null filtering tests', function() {

  var env = Helpers.defaultEnv();

  beforeEach(async function() {
    await env.db.schema.createTable('testTable', function(table) {
      table.increments();
      table.text('name');
      table.integer('value');
    });
    await env.db('testTable').insert([
      { name: 'toto', value: 42 },
      { name: 'titi', value: 43 },
      { name: 'titi', value: null },
      { name: null, value: 44 },
      { name: null, value: null }
    ]);
  });

  describe('items filtering', function() {
    _.forEach([
      {
        query: { query: JSON.stringify({ name: null }) },
        reply: [
          { id: 4, name: null, value: 44 },
          { id: 5, name: null, value: null }
        ]
      },
      {
        query: { query: JSON.stringify({ value: null }) },
        reply: [
          { id: 3, name: 'titi', value: null },
          { id: 5, name: null, value: null }
        ]
      },
      {
        query: { query: JSON.stringify({ name: { $not: null } }) },
        reply: [
          { id: 1, name: 'toto', value: 42 },
          { id: 2, name: 'titi', value: 43 },
          { id: 3, name: 'titi', value: null }
        ]
      },
      {
        query: { query: JSON.stringify({ value: { $not: null } }) },
        reply: [
          { id: 1, name: 'toto', value: 42 },
          { id: 2, name: 'titi', value: 43 },
          { id: 4, name: null, value: 44 }
        ]
      },
      {
        query: { query: JSON.stringify({ value: { $eq: null } }) },
        reply: [
          { id: 3, name: 'titi', value: null },
          { id: 5, name: null, value: null }
        ]
      },
      {
        query: { query: JSON.stringify({ value: { $ne: null } }) },
        reply: [
          { id: 1, name: 'toto', value: 42 },
          { id: 2, name: 'titi', value: 43 },
          { id: 4, name: null, value: 44 }
        ]
      },
      {
        query: { query: JSON.stringify({ value: { $gt: null } }) },
        reply: [
          { id: 1, name: 'toto', value: 42 },
          { id: 2, name: 'titi', value: 43 },
          { id: 4, name: null, value: 44 }
        ]
      },
      {
        query: { query: JSON.stringify({ value: { $gte: null } }) },
        reply: [
          { id: 1, name: 'toto', value: 42 },
          { id: 2, name: 'titi', value: 43 },
          { id: 3, name: 'titi', value: null },
          { id: 4, name: null, value: 44 },
          { id: 5, name: null, value: null }
        ]
      },
      {
        query: { query: JSON.stringify({ value: { $lt: null } }) },
        reply: [
          { id: 1, name: 'toto', value: 42 },
          { id: 2, name: 'titi', value: 43 },
          { id: 4, name: null, value: 44 }
        ]
      },
      {
        query: { query: JSON.stringify({ value: { $lte: null } }) },
        reply: [
          { id: 1, name: 'toto', value: 42 },
          { id: 2, name: 'titi', value: 43 },
          { id: 3, name: 'titi', value: null },
          { id: 4, name: null, value: 44 },
          { id: 5, name: null, value: null }
        ]
      },
      {
        query: { query: JSON.stringify({ value: { $in: null } }) },
        reply: [
          { id: 1, name: 'toto', value: 42 },
          { id: 2, name: 'titi', value: 43 },
          { id: 3, name: 'titi', value: null },
          { id: 4, name: null, value: 44 },
          { id: 5, name: null, value: null }
        ]
      },
      {
        query: { query: JSON.stringify({ value: { $nin: null } }) },
        reply: [
          { id: 1, name: 'toto', value: 42 },
          { id: 2, name: 'titi', value: 43 },
          { id: 3, name: 'titi', value: null },
          { id: 4, name: null, value: 44 },
          { id: 5, name: null, value: null }
        ]
      },
      {
        query: { query: JSON.stringify({ value: { $like: null } }) },
        reply: [
          { id: 3, name: 'titi', value: null },
          { id: 5, name: null, value: null }
        ]
      },
      {
        query: { query: JSON.stringify({ $and: [ { name: { $eq: null } }, { value: null } ] }) },
        reply: [
          { id: 5, name: null, value: null }
        ]
      },
      {
        query: { query: JSON.stringify({ $or: [ { name: { $not: null } }, { value: { $ne: null } } ] }) },
        reply: [
          { id: 1, name: 'toto', value: 42 },
          { id: 2, name: 'titi', value: 43 },
          { id: 3, name: 'titi', value: null },
          { id: 4, name: null, value: 44 }
        ]
      }
    ], function(params) {
      it(`can filter items with: ${JSON.stringify(params.query)}`, async function() {
        (new KnexREST(env.mgr, 'testTable', { key: 'id' })).register(env.app);

        await sa.get('localhost:1234/testTable')
        .query(params.query)
        .then((ret) => expect(ret.body).eql(params.reply));
      });
    });
  });
});
