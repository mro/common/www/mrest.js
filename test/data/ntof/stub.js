const
  _ = require('lodash'),
  fs = require('fs'),
  debug = require('debug')('app:server'),
  express = require('express'),
  path = require('path'),
  knex = require('knex').default,
  createSchema = require('./db-desc.js'),
  // @ts-ignore
  dbInfo = require('./dbInfo'),
  // @ts-ignore
  swaggerInfo = require('./swaggerInfo'),
  KnexREST = require('../../../src');

const app = express();

const config = {
  client: 'sqlite3',
  useNullAsDefault: true,
  connection: { filename: path.join(__dirname, 'db.sql') },
  definition: { info: { title: "ntof stub", version: "1.0" } }
};
if (fs.existsSync(config.connection.filename)) {
  fs.unlinkSync(config.connection.filename);
}

async function createDB() {
  const db = knex(config);
  await db.transaction(async function(tr) {
    await createSchema(tr);

    const data = fs.readFileSync(path.join(__dirname, '..', 'testFile.txt'), 'utf8');
    const lines = _.compact(_.map(data.split(';\n'), _.trim));

    for (let i = 0; i < lines.length; i++) {
      await tr.raw(lines[i]);
    }
    debug('created');
  });
  await db.destroy();
}

if (require.main === module) {
  /* we're called as a main, let's listen */
  debug("Starting server");

  createDB()
  .then(() => {
    app.use('/db', KnexREST.router(config, dbInfo, swaggerInfo));
    app.listen(8080, () => console.log("Server is now listening on http://localhost:8080/db/api-docs"));
  })
  .catch((err) => console.log(err));
}
else {
  /* export our server */
  module.exports = app;
}
