insert or replace into `RUN_STATUS` (`RUN_STATUS`) values ('unknown');

insert or replace into `RUN_DATA_STATUS` (`RUN_DATA_STATUS`) values ('unknown');

insert or replace into `RUNS` (`RUN_CASTOR_FOLDER`, `RUN_DATA_STATUS`, `RUN_DESCRIPTION`, `RUN_DESCRIPTION_ORIGINAL`, `RUN_DET_SETUP_ID`, `RUN_ID`, `RUN_MAT_SETUP_ID`, `RUN_RUN_NUMBER`, `RUN_RUN_STATUS`, `RUN_START`, `RUN_STOP`, `RUN_TITLE`, `RUN_TITLE_ORIGINAL`, `RUN_TOTAL_PROTONS`) values ('No information', 'unknown', '534 2000-12-31T21:59:00.000Z', '534 2000-12-31T21:59:00.000Z', 1, 534, 1, 489, 'unknown', '2000-12-31 22:59:00.000', '2000-12-31 23:00:00.000', 'No Title', 'No Title', 0);

insert or replace into `MATERIALS_TYPE` (`MAT_TYPE`) values ('sample');

insert or replace into `MATERIALS` (`MAT_AREA_DENSITY_UM_CM2`, `MAT_ATOMIC_NUMBER`, `MAT_COMPOUND`, `MAT_DESCRIPTION`, `MAT_DIAMETER_MM`, `MAT_ID`, `MAT_MASS_MG`, `MAT_MASS_NUMBER`, `MAT_STATUS`, `MAT_THICKNESS_UM`, `MAT_TITLE`, `MAT_TYPE`) values (NULL, NULL, 'Empty', ' &lt;p&gt;Nothing in beam, not even a frame&lt;/p&gt; ', NULL, 13, NULL, 0, 1, NULL, 'Nothing in beam, not even a frame', 'sample');

insert or replace into `REL_MATERIALS_SETUPS` (`MAT_ID`, `MAT_POSITION`, `MAT_SETUP_ID`, `MAT_STATUS`) values (13, NULL, 1, 1);

insert or replace into `OPERATION_PERIODS` (`OPER_START`, `OPER_STOP`, `OPER_YEAR`) values ('2000-12-31 22:59:00.000', '2000-12-31 23:00:00.000', 2000);

insert or replace into `EXPERIMENTAL_AREAS` (`EAR_NAME`, `EAR_NUMBER`) values ('EAR1', 1);

insert or replace into `MATERIALS_SETUPS` (`MAT_SETUP_DESCRIPTION`, `MAT_SETUP_EAR_NUMBER`, `MAT_SETUP_ID`, `MAT_SETUP_NAME`, `MAT_SETUP_OPER_YEAR`) values ('Material setup generated during database migration', 1, 1, '2000 EAR1 Samples: Empty', 2000);

insert or replace into `DETECTORS_SETUPS` (`DET_SETUP_DESCRIPTION`, `DET_SETUP_EAR_NUMBER`, `DET_SETUP_ID`, `DET_SETUP_NAME`, `DET_SETUP_OPER_YEAR`) values ('Unknown detectors setup, further details may be found in run''s associated documents', 1, 1, '2000 EAR1 -- Unknown', 2000);

