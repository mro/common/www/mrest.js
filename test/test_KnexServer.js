// @ts-check
const
  { describe, it, beforeEach } = require('mocha'),
  { expect } = require('chai'),
  { assign, noop, get } = require('lodash'),
  { Router } = require('express'),
  Helpers = require('./Helpers');

var KnexServer = require('../src/KnexServer'); /* will start the server */

describe('KnexServer tests', function() {

  var env = Helpers.defaultEnv();

  beforeEach(async function() {
    await env.db.schema.createTable('testTable',
      function(/** @type {any} */ table) {
        table.increments();
        table.text('name');
        table.integer('value');
      });
    await env.db('testTable').insert([
      { name: 'toto', value: 42 }
    ]);
  });

  it('can share a connection', async function() {
    var server = new KnexServer(env.dbConfig, { testTable: { table: 'test' } }, {});
    var server2 = new KnexServer(assign({ knex: server.knex }, env.dbConfig),
      { testTable: { table: 'test' } }, {});

    server.register(env.app);
    server2.register(Router()); /* jshint ignore:line */

    expect(server.knex).to.equal(server2.knex);

    server2.release();
    server.release();
  });

  it('can rollback during a transaction', async function() {
    var server = new KnexServer(env.dbConfig, { testTable: { table: 'testTable' } }, {});
    server.register(env.app);

    try {
      // Generate a transaction knex object
      await server.knex.transaction(async (/** @type {KnexServer.Knex} */ knexTransaction) => {
        // Do an insert on table
        await server.tables['testTable'].insert(// @ts-ignore fake Request
          { get: noop, body: { name: 'test', value: 100 }, knexTransaction }, { set: noop });
        // Check that the new row is there
        const newId = await server.tables['testTable'].search(// @ts-ignore fake Request
          { get: noop, query: { max: 1, filter: `name:test` }, knexTransaction }, { set: noop })
        .then((ret) => get(ret, [ 0, 'id' ]));
        expect(newId).to.equal(2);

        // Do a wrong insert now (so will throw)
        await server.tables['testTable'].insert(// @ts-ignore fake Request
          { get: noop, body: { wrongField: 'wrong' }, knexTransaction }, { set: noop });
      });
    }
    catch (error) {
      // Tx is automatically rollbacked here.
    }

    // Check that the first inserted line is not there anymore
    const newId = await server.tables['testTable'].search(// @ts-ignore fake Request
      { get: noop, query: { max: 1, filter: `name:test` } }, { set: noop })
    .then((ret) => get(ret, [ 0, 'id' ]));
    expect(newId).to.be.undefined();

    server.release();
  });
});
