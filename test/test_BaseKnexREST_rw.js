// @flow
const
  { describe, it, beforeEach } = require('mocha'),
  { expect } = require('chai'),
  _ = require('lodash'),
  sa = require('superagent'),
  Helpers = require('./Helpers');

var KnexREST = require('../src/KnexREST'); /* will start the server */

describe('KnexREST rw tests', function() {

  var env = Helpers.defaultEnv();

  beforeEach(async function() {
    await env.db.schema.createTable('testTable', function(table) {
      table.increments();
      table.text('name').notNullable();
      table.integer('value');
    });
    await env.db('testTable').insert([
      { name: 'toto', value: 42 },
      { name: 'titi', value: 43 },
      { name: 'titi', value: 44 }
    ]);
  });

  it('can insert an item', async function() {
    var service = new KnexREST(env.mgr, 'testTable', { key: 'id', rw: true });
    service.register(env.app);
    KnexREST.register(env.app); /* base services */

    var ret = await sa.post('localhost:1234/testTable')
    .send({ name: 'test', value: 24 });

    expect(ret).to.deep.include({ status: 200 });
    expect(ret.body).to.equal(4);

    ret = await sa.get('localhost:1234/testTable')
    .query({ filter: 'name:test' });
    expect(ret).to.deep.include({ status: 200 });
    expect(ret.body).to.have.length(1);
    expect(ret.body[0]).to.deep.include({ id: 4, name: 'test', value: 24 });
  });

  it('can insert several items', async function() {
    var service = new KnexREST(env.mgr, 'testTable', { key: 'id', rw: true });
    service.register(env.app);
    KnexREST.register(env.app); /* base services */

    var ret = await sa.post('localhost:1234/testTable')
    .send([ { name: 'test', value: 24 }, { name: 'test', value: 25 } ]);

    expect(ret).to.deep.include({ status: 200 });
    expect(ret.body).to.deep.equal(5);

    ret = await sa.get('localhost:1234/testTable')
    .query({ filter: 'name:test', sort: 'value:asc' });
    expect(ret).to.deep.include({ status: 200 });
    expect(ret.body).to.have.length(2);
    expect(ret.body[0]).to.include({ name: 'test', value: 24 });
    expect(ret.body[1]).to.include({ name: 'test', value: 25 });
  });

  it('fails to create invalid items', async function() {
    var service = new KnexREST(env.mgr, 'testTable', { key: 'id', rw: true });
    service.register(env.app);
    KnexREST.register(env.app); /* base services */

    await sa.post('localhost:1234/testTable')
    .send({ value: 24 })
    .then(
      () => { throw 'should fail'; },
      _.noop);
  });

  it('can update an item', async function() {
    var service = new KnexREST(env.mgr, 'testTable', { key: 'id', rw: true });
    service.register(env.app);
    KnexREST.register(env.app); /* base services */

    var ret = await sa.put('localhost:1234/testTable/item/1')
    .send({ name: 'test', value: 1234 });

    expect(ret).to.deep.include({ status: 200 });

    ret = await sa.get('localhost:1234/testTable/item/1');
    expect(ret.body).to.deep.equal({ id: 1, name: 'test', value: 1234 });
  });

  it('can delete an item', async function() {
    var service = new KnexREST(env.mgr, 'testTable', { key: 'id', rw: true });
    service.register(env.app);
    KnexREST.register(env.app); /* base services */

    var ret = await sa.delete('localhost:1234/testTable/item/1');
    expect(ret).to.deep.include({ status: 200 });

    await sa.get('localhost:1234/testTable/item/1')
    .then(() => { throw "should fail"; }, _.noop);
  });

  it('fails on readonly database', async function() {
    var service = new KnexREST(env.mgr, 'testTable', { key: 'id' });
    service.register(env.app);
    KnexREST.register(env.app); /* base services */

    await sa.post('localhost:1234/testTable')
    .send({ name: 'test', value: 24 })
    .then(() => { throw "should fail"; }, _.noop);

    await sa.put('localhost:1234/testTable/item/1')
    .send({ name: 'test', value: 1234 })
    .then(() => { throw "should fail"; }, _.noop);
  });

  it('can post and delete on db without ids', async function() {
    var service = new KnexREST(env.mgr, 'testTable', { rw: true });
    service.register(env.app);
    KnexREST.register(env.app); /* base services */

    var ret = await sa.post('localhost:1234/testTable')
    .send({ name: 'test', value: 24 });

    expect(ret).to.deep.include({ status: 200 });
    expect(ret.body).to.equal(null); // can't return an id

    ret = await sa.delete('localhost:1234/testTable')
    .query({ query: '{"value":{"$gte":43}}' });
    expect(ret).to.deep.include({ status: 200 });

    ret = await sa.get('localhost:1234/testTable')
    .query({ sort: 'value:asc' });
    expect(ret).to.deep.include({ status: 200 });
    expect(ret.body).to.have.length(2);
    expect(ret.body[0]).to.include({ name: 'test', value: 24 });
    expect(ret.body[1]).to.include({ name: 'toto', value: 42 });
  });
});
