// @ts-check
const
  { describe, it, beforeEach, afterEach } = require('mocha'),
  _ = require('lodash'),
  fs = require('fs'),
  { expect } = require('chai'),
  sa = require('superagent'),
  Helpers = require('./Helpers'),
  createSchema = require('./data/ntof/db-desc'),
  // @ts-ignore
  dbInfo = require('./data/ntof/dbInfo'),
  // @ts-ignore
  swaggerInfo = require('./data/ntof/swaggerInfo'),
  KnexServer = require('../src');

describe('dbInfo', function() {
  /** @type { ReturnType<typeof Helpers.defaultEnv> & {
   *    knexServer: KnexServer,
   *    data: any,
   *    dbInfo: any
   *  }}
   */ // @ts-ignore
  const env = Helpers.defaultEnv();

  beforeEach(async function() {
    env.knexServer = new KnexServer(env.dbConfig, dbInfo, swaggerInfo);
    env.knexServer.register(env.app);
    await createSchema(env.db);

    env.data = fs.readFileSync('test/data/ntof/dbInfo.json');
    env.dbInfo = JSON.parse(env.data.toString());

    const data = fs.readFileSync('test/data/testFile.txt', 'utf8');
    // $FlowIgnore: map works on strings
    const lines = _.compact(_.map(data.split(';\n'), _.trim));

    for (let i = 0; i < lines.length; i++) {
      await env.db.raw(lines[i]);
    }
  });

  afterEach(function() {
    env.knexServer.release();
  });

  // eslint-disable-next-line complexity
  it('dumps all tables', async function() {
    for (const info in env.dbInfo) {
      if (!env.dbInfo.hasOwnProperty(info)) { continue; }

      var ret = await sa.get('http://localhost:1234/' + info);
      expect(ret).to.deep.include({ status: 200 });

      const related = env.dbInfo[info].related;
      const key = env.dbInfo[info].key;
      const idKey = env.dbInfo[info].mapping[key];
      // eslint-disable-next-line max-len
      const firstItem = ret.body[Object.keys(ret.body)[0]];
      if (!_.isEmpty(related) && !_.isEmpty(firstItem)) {
        const id = firstItem[idKey];
        for (const rel in related) {
          if (!related.hasOwnProperty(rel)) { continue; }

          if (!_.isNil(id) && id !== 'unknown') {
            // eslint-disable-next-line max-len
            ret = await sa.get('http://localhost:1234/' + info + '/item/' + id + '/related/' + rel);
            expect(ret).to.deep.include({ status: 200 });
          }
        }
      }
    }
  });
});
