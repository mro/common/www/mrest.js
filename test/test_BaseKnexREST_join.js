
const
  express = require('express'),
  { describe, it, beforeEach } = require('mocha'),
  { expect } = require('chai'),
  sa = require('superagent'),
  Helpers = require('./Helpers');

var KnexREST = require('../src/KnexREST'); /* will start the server */

describe('KnexREST db join relations', function() {
  /** @type { ReturnType<typeof Helpers.defaultEnv> & {
   *    infoDesc: mrest.JoinTableRelation
   *  }}
   */ // @ts-ignore
  var env = Helpers.defaultEnv();

  beforeEach(function() {
    env.infoDesc = {
      path: 'infoTable',
      join: {
        table: 'joinTable',
        key: 'testID',
        relKey: 'infoID'
      }
    };
  });

  beforeEach(async function() {
    await env.db.schema.createTable('testTable', function(table) {
      table.increments();
      table.text('name');
      table.integer('value');
    });
    await env.db('testTable').insert([
      { id: 1, name: 'toto', value: 42 },
      { id: 2, name: 'titi', value: 43 },
      { id: 3, name: 'titi', value: 44 }
    ]);
  });

  beforeEach(async function() {
    await env.db.schema.createTable('infoTable', function(table) {
      table.increments();
      table.text('info');
    });
    await env.db('infoTable').insert([
      { id: 1, info: 'info1' },
      { id: 2, info: 'info2' }
    ]);
  });

  beforeEach(async function() {
    await env.db.schema.createTable('joinTable', function(table) {
      table.integer('testID');
      table.integer('infoID');
    });
    await env.db('joinTable').insert([
      { testID: 3, infoID: 1 },
      { testID: 3, infoID: 2 },
      { testID: 2, infoID: 1 }
    ]);
  });

  it('can list relations', async function() {
    (new KnexREST(env.mgr, 'infoTable', {})).register(env.app);
    (new KnexREST(env.mgr, 'testTable',
      { key: 'id', related: { info: env.infoDesc } })).register(env.app);

    const ret = await sa.get('localhost:1234/testTable/item/2/related');
    expect(ret).to.deep.include({ status: 200, body: [ 'info' ] });
  });

  it('can list items using a relation', async function() {
    (new KnexREST(env.mgr, 'infoTable', {})).register(env.app);
    (new KnexREST(env.mgr, 'testTable',
      { key: 'id', related: { info: env.infoDesc } })).register(env.app);

    const ret = await sa.get('localhost:1234/testTable/item/3/related/info');
    expect(ret).to.deep.include({ status: 200, body: [
      { id: 1, info: 'info1' },
      { id: 2, info: 'info2' }
    ] });
  });

  it('can filter items in a relation', async function() {
    var rt = express.Router();
    (new KnexREST(env.mgr, 'infoTable', {})).register(rt);
    (new KnexREST(env.mgr, 'testTable',
      { key: 'id', related: { info: env.infoDesc } })).register(rt);
    env.app.use('/pwet', rt);

    const ret = await sa.get('localhost:1234/pwet/testTable/item/3/related/info')
    .query({ filter: { info: 'info1' } });
    expect(ret).to.deep.include({ status: 200, body: [
      { id: 1, info: 'info1' }
    ] });
  });

  it('can count items in a relation', async function() {
    (new KnexREST(env.mgr, 'infoTable', {})).register(env.app);
    (new KnexREST(env.mgr, 'testTable',
      { key: 'id', related: { info: env.infoDesc } })).register(env.app);

    const ret = await sa.get('localhost:1234/testTable/item/3/related/info/count');
    expect(ret).to.deep.include({ status: 200, body: 2 });
  });

  it('can modify a relation', async function() {
    (new KnexREST(env.mgr, 'infoTable', {})).register(env.app);
    (new KnexREST(env.mgr, 'testTable',
      { rw: true, key: 'id', related: { info: env.infoDesc } })).register(env.app);

    var ret = await sa.post('localhost:1234/testTable/item/1/related/info')
    // @ts-ignore
    .type('json').send(1);
    expect(ret).to.deep.include({ status: 200 });

    ret = await sa.get('localhost:1234/testTable/item/1/related/info');
    expect(ret).to.deep.include({ status: 200, body: [
      { id: 1, info: 'info1' }
    ] });

    ret = await sa.delete('localhost:1234/testTable/item/1/related/info')
    // @ts-ignore
    .type('json').send(1);

    ret = await sa.get('localhost:1234/testTable/item/1/related/info');
    expect(ret).to.deep.include({ status: 200, body: [ ] });
  });

  it('can modify a relation list', async function() {
    (new KnexREST(env.mgr, 'infoTable', {})).register(env.app);
    (new KnexREST(env.mgr, 'testTable',
      { rw: true, key: 'id', related: { info: env.infoDesc } })).register(env.app);

    var ret = await sa.post('localhost:1234/testTable/item/1/related/info')
    .send([ 1, 2 ]);
    expect(ret).to.deep.include({ status: 200 });

    ret = await sa.get('localhost:1234/testTable/item/1/related/info');
    expect(ret).to.deep.include({ status: 200, body: [
      { id: 1, info: 'info1' }, { id: 2, info: 'info2' }
    ] });

    ret = await sa.delete('localhost:1234/testTable/item/1/related/info')
    .send([ 1, 2 ]);

    ret = await sa.get('localhost:1234/testTable/item/1/related/info');
    expect(ret).to.deep.include({ status: 200, body: [ ] });
  });
});
