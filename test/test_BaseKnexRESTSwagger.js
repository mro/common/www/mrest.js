// @ts-check
const
  { describe, it } = require('mocha'),
  { expect } = require('chai'),
  Helpers = require('./Helpers');

var KnexREST = require('../src/KnexREST');
var KnexRESTSwagger = require('../src/KnexRESTSwagger');

describe('KnexREST docs', function() {
  var env = Helpers.defaultEnv();

  it('can generate doc on empty swagger', function() {
    var srv = new KnexREST(env.mgr, 'testTable', {
      key: 'id',
      related: {
        info: {
          path: 'infoTable',
          key: 'testID'
        }
      }
    });
    var dbSwagger = new KnexRESTSwagger();
    dbSwagger.swag(srv);
  });

  it('can generate doc on complete swagger', function() {
    var srv = new KnexREST(env.mgr, 'testTable', {
      key: 'id',
      related: {
        info: {
          path: 'infoTable',
          key: 'testID'
        }
      }
    });
    const dbSwagger = new KnexRESTSwagger({ definition: { info: { title: "sample", version: "1.0.0" } } }, {});
    const doc = dbSwagger.swag(srv);
    expect(doc).to.have.nested.property('paths./testTable.get');
  });
});
