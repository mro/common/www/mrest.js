const KnexTransactionManager = require('../src/KnexTransactionManager');
const
  { beforeEach, afterEach } = require('mocha'),
  debug = require('debug')('tests'),
  tmp = require('tmp'),
  path = require('path'),
  knex = require('knex').default,
  express = require('express');


module.exports = {
  defaultEnv() {
    /** @type {{
      *   app: express.Express & { sock?: import('http').Server },
      *   db: import('knex').Knex,
      *   mgr: KnexTransactionManager,
      *   dir: tmp.DirResult,
      *   dbConfig: any
      * }}
      */ // @ts-ignore
    var env = {};

    /* app setup */
    beforeEach(function(done) {
      env.app = express();
      env.app.sock = env.app.listen(1234, 'localhost', () => done());
    });

    /* db setup */
    beforeEach(function() {
      env.dir = tmp.dirSync({ unsafeCleanup: true });
      debug('tmpDir:', env.dir.name);
      var config = {
        client: 'sqlite3',
        useNullAsDefault: true,
        connection: { filename: path.join(env.dir.name, 'db.sql') }
      };
      env.db = knex(config);
      env.mgr = new KnexTransactionManager(env.db);
      env.dbConfig = config;
    });

    afterEach(function() {
      env.app?.sock?.close();
      // @ts-ignore
      delete env.app;

      env.dir?.removeCallback();
      // @ts-ignore
      delete env.dir;
      return env.db?.destroy()
      .then(() => {
        // @ts-ignore
        delete env.db;
      });
    });

    return env;
  }
};
