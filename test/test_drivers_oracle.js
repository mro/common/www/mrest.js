// @ts-check
const
  { describe, it, afterEach } = require('mocha'),
  { expect } = require('chai'),
  knex = require('knex').default;

describe('oracle extensions', function() {
  /** @type {{ db?: import('knex').Knex }} */
  var env = {};

  afterEach(async function() {
    await env.db?.destroy();
    delete env.db;
  });

  it('uses binary_ai collation with like operator', async function() {
    env.db = knex({ client: 'oracledb' });

    expect(env.db?.('RUNS').where('RUN_TITLE', 'like', '%test%').toString())
    .to.contain('binary_ai');

    expect(env.db?.('RUNS').whereLike('RUN_TITLE', '%test%').toString())
    .to.contain('binary_ai');
  });
});
